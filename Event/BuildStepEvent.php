<?php
namespace Brown298\ReportBuilderBundle\Event;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Doctrine\ORM\EntityManager;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Symfony\Component\EventDispatcher\Event;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\StepInterface;

/**
 * BuildStepEvent
 *
 */
class BuildStepEvent extends Event
{
    /**
     * @var BuiltReport
     */
    protected $report;

    /**
     * @var object instance of AbstractStep
     */
    protected $step;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SecurityInterface
     */
    private $security;

    /**
     * @var string
     */
    private $class;

    /**
     * Constructor
     *
     * @param BuiltReport $report
     * @param StepInterface $step
     * @param \Doctrine\ORM\EntityManager $em
     * @param SecurityInterface $security
     * @param \Brown298\ReportBuilderBundle\Mapping\MetaData\ClassMetaData $classMeta
     */
    public function __construct(
        BuiltReport       $report,
        StepInterface     $step,
        EntityManager     $em,
        SecurityInterface $security,
                          $class = null
    ) {
        $this->report   = $report;
        $this->step     = $step;
        $this->em       = $em;
        $this->security = $security;
        $this->class    = $class;
    }

    /**
     * getReport
     *
     * @return BuiltReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * getStep
     *
     * @return StepInterface
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * getEm
     *
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * getSecurity
     *
     * @return SecurityInterface
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * getClass
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }
}