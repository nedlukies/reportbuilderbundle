<?php
namespace Brown298\ReportBuilderBundle\DataTables;

use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\ReportBuilderBundle\Entity\Report;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class ReportDataTable
 * @package Brown298\ReportBuilderBundle\DataTables
 */
class ReportDataTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface
{

    /**
     * @var array
     */
    protected $columns = array();

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface
     */
    protected $report;

    /**
     * @var array
     */
    protected $parameterData;

    /**
     * @var null if set this will force a limit on the
     */
    protected $filterLimit = null;

    /**
     * @var bool
     */
    protected $preview = false;

    /**
     * @param ReportContainerInterface $report
     */
    public function __construct(ReportContainerInterface $report)
    {
        $this->report  = $report;
        $this->columns = $report->getColumns();
    }

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb     = $this->report->createQueryBuilder();
        $params = $qb->getParameters();

        if (is_array($this->parameterData)) {
            foreach ($this->parameterData as $n => $v) {

                if (empty($v)) {
                    continue;
                }

                /**
                 * @var int $i
                 * @var \Doctrine\ORM\Query\Parameter $d
                 */
                foreach ($params as $i => $d) {

                    if ($d->getValue() == ':' . $n) {
                        $d->setValue($v);
                    }

                    if (!is_object($d->getValue()) && trim($d->getValue(), '%') == ':' . $n) {
                        $d->setValue(str_replace(':' . $n, $v, $d->getValue()));
                    }
                }
            }
        }

        foreach ($params as $i => $d) {
            // remove param if no value is set
            if (
                preg_match('/^:.+/', $d->getValue())
                || preg_match('/^\%:.+/', $d->getValue())
            ) {
                /** @var \Doctrine\ORM\Query\Expr\Andx $wheres */
                $wheres = $qb->getDQLPart('where');
                $qb->resetDQLPart('where');
                $parts  = $wheres->getParts();
                /** @var \Doctrine\ORM\Query\Expr\Comparison $part */
                foreach ($parts as $part) {
                    if (!preg_match('/\?' . $d->getName() . '/', $part->getRightExpr())) {
                        // keep this value
                        $qb->andWhere($part);
                    }
                }

                unset($params[$i]);
            }
        }

        $qb->setParameters($params);

        if (is_int($this->getFilterLimit()) && $this->getFilterLimit() > 0) {
            $qb->setMaxResults($this->getFilterLimit());
        }

        return $qb;
    }

    /**
     * @return null
     */
    public function getFilterLimit()
    {
        return $this->filterLimit;
    }

    /**
     * @param null $filterLimit
     */
    public function setFilterLimit($filterLimit)
    {
        $this->filterLimit = $filterLimit;
    }

    /**
     * {@inheritDoc}
     */
    public function processRequest(Request $request, \Closure $dataFormatter = null)
    {
        $this->parameterData = $request->get('report_parameters', null);

        return parent::processRequest($request, $dataFormatter);
    }

    /**
     * getDataFormatter
     *
     * @return \Closure
     */
    public function getDataFormatter()
    {
        return function($data) {
            $count   = 0;
            $results = array();

            foreach ($data as $row) {
                foreach ($row as $name=>$col) {
                    if (is_object($col)) {
                        if ($col instanceof \DateTime) {
                            $results[$count][] = $col->format('m/d/Y H:i:s');
                        } else {
                            $results[$count][] = $col->__toString();
                        }
                    } else {
                        $results[$count][] = $col;
                    }
                }
                $count += 1;
            }

            return $results;
        };
    }

    /**
     * @return boolean
     */
    public function isPreview()
    {
        return $this->preview;
    }

    /**
     * @param boolean $preview
     */
    public function setPreview($preview = true)
    {
        $this->preview = $preview;
    }

    /**
     * execute
     *
     * @param $service
     * @param $formatter
     * @return mixed
     */
    public function execute($service, $formatter)
    {
        $service->setPreview($this->isPreview());
        return $service->process($formatter, $this->hydrateObjects);
    }

    /**
     * getJsonResponse
     *
     * @param Request $request
     *
     * @param callable|null $dataFormatter
     *
     * @return JsonResponse
     */
    public function getJsonResponse(Request $request, \Closure $dataFormatter = null)
    {
        try {
            return new JsonResponse($this->getData($request, $dataFormatter));
        } catch (\Exception $ex) {
            return new JsonResponse(array(
                'error'         => $ex->getMessage(),
                'sEcho'         => 0,
                'iTotalRecords' => 0,
                'aaData'        => array(),
                'iTotalDisplayRecords' => 0,
            ));
        }
    }
}