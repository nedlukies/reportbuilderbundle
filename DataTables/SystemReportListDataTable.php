<?php
namespace Brown298\ReportBuilderBundle\DataTables;

use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class SystemReportListDataTable
 * @package Brown298\ReportBuilderBundle\DataTables
 */
class SystemReportListDataTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface
{
    /**
     * @var array
     */
    protected $columns = array(
        'r.name'   => 'Name',
        'r.id'     => 'Actions',
    );

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * @var bool
     */
    protected $includeDeleted = false;

    /**
     * @param bool $includeDeleted
     */
    public function __construct($includeDeleted = false)
    {
        $this->includeDeleted = $includeDeleted;
    }

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $reportRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('Brown298\ReportBuilderBundle\Entity\Report');
        $qb = $reportRepository->getSystemForIndexQb($this->includeDeleted);

        return $qb;
    }

    /**
     * getDataFormatter
     *
     * @return \Closure
     */
    public function getDataFormatter()
    {
        $renderer = $this->container->get('templating');
        return function($data) use ($renderer) {
            $count   = 0;
            $results = array();

            foreach ($data as $row) {
                $results[$count][] = $row->getName();
                $results[$count][] = $renderer->render('Brown298ReportBuilderBundle:Index:BuiltReportList/actions.html.twig', array('report' => $row));
                $count += 1;
            }

            return $results;
        };
    }
}
