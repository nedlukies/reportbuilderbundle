<?php
namespace Brown298\ReportBuilderBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * ExportController
 *
 * @Route("/reports/{reportId}/export")
 */
class ExportController extends AbstractController
{
    /**
     * @Route("/{report}", name="reports_export_index", defaults={"report" = null})
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function indexAction(Request $request)
    {
        $this->assertValidReport();
        $this->checkReportPermission('VIEW');

        $reportContainer = $this->getReportContainer();
        $stepService     = $this->get('brown298.report_builder.build.step');
        $security        = $this->getSecurity();
        $tableService    = $this->get('brown298.report_builder.table');
        $em              = $this->get('doctrine.orm.entity_manager');
        $container       = $this->get('service_container');
        $report          = $reportContainer->getReport();
        $templateService = $this->get('brown298.report_builder.template.step');

        if ($report->isBuilt()) {
            $this->assertBuildEnabled();

            if (!$stepService->isViewable()) {
                throw $this->createNotFoundException('Unable to find Report');
            }

            
        }

        $tmpfile = tmpfile();
        
        fputcsv($tmpfile, $reportContainer->getColumns(), ",", '"');
        
        foreach ($reportContainer->getDataRows() as $row) {
            fputcsv($tmpfile, $row, ",", '"');
        }
        
        fseek($tmpfile, 0);
        
        $content = stream_get_contents($tmpfile);
        return new Response($content,200,array(
              'Content-type'=>'application/octet-stream',
              'Expires' => "Sat, 01 Jan 2000 00:00:00 GMT",
              "Last-Modified" => gmdate("D, d M Y H:i:s") . " GMT",
              'Pragma' => 'public',
              'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
              'Content-Disposition' => 'attachment; filename="' . $reportContainer->getName() . '.csv";',
              'Content-length' => strlen($content)
        ));
        
        //return $this->render($templateService->getViewTemplate(), $params);
    }
}