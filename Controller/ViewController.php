<?php
namespace Brown298\ReportBuilderBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * ViewController
 *
 * @Route("/reports/{reportId}/view")
 */
class ViewController extends AbstractController
{
    /**
     * @Route("/{report}", name="reports_view_index", defaults={"report" = null})
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function indexAction(Request $request)
    {
        $this->assertValidReport();
        $this->checkReportPermission('VIEW');

        $reportContainer  = $this->getReportContainer();
        $stepService      = $this->get('brown298.report_builder.build.step');
        $security         = $this->getSecurity();
        $tableService     = $this->get('brown298.report_builder.table');
        $em               = $this->get('doctrine.orm.entity_manager');
        $container        = $this->get('service_container');
        $report           = $reportContainer->getReport();
        $templateService  = $this->get('brown298.report_builder.template.step');
        $hasParameterForm = false;
        $parameterForm    = null;

        if ($report->isBuilt()) {
            $this->assertBuildEnabled();

            if (!$stepService->isViewable()) {
                throw $this->createNotFoundException('Unable to find Report');
            }

            $steps = $stepService->getSteps();

            $hasParameterForm = $reportContainer->hasParameters();
            if ($hasParameterForm) {
                $parameterForm = $reportContainer->getParameterForm();

                if($request->getMethod() == 'POST') {
                    $parameterForm->handleRequest($request);
                }
            }
        }

        // Instantiate table object
        $tableService->setReportContainer($reportContainer);

        $dataTable = $tableService->getDataTable();
        $dataTable->setEm($em);
        $dataTable->setContainer($container);
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }

        $canUpdate   = $report->isBuilt()    && $security->hasPermissions('UPDATE');
        $canDelete   = !$report->isDeleted() && $security->hasPermissions('DELETE');
        $canUndelete = $report->isDeleted()  && $security->hasPermissions('UNDELETE');

        $qb     = $dataTable->getQueryBuilder($request);
        $query  = $qb->getQuery();
        $sql    = $query->getSql();
        $params = $query->getParameters();

        if ($parameterForm != null) {
            $parameterForm = $parameterForm->createView();
        }

        $params = compact('report', 'tableService', 'steps', 'canUpdate', 'canDelete', 'canUndelete', 'dataTable', 'parameterForm', 'hasParameterForm', 'sql', 'params');
        return $this->render($templateService->getViewTemplate(), $params);
    }
}