<?php
namespace Brown298\ReportBuilderBundle\Controller;

use Exception;
use Brown298\ReportBuilderBundle\Event\BuildStepEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * BuildController
 *
 * @Route("/reports/{reportId}/build")
 */
class BuildController extends AbstractController
{
    /**
     * Redirect to next action
     *
     * @Route("", name="reports_build_index")
     */
    public function indexAction()
    {
        $this->assertBuildEnabled();
        $this->assertValidBuiltReport();
        $this->checkReportPermission('UPDATE');

        $reportContainer = $this->getReportContainer();
        $report          = $reportContainer->getReport();

        $route = 'reports_build_next';
        if ($report->isPublished()) {
            $route = 'reports_build_first';
        }

        $url = $this->generateUrl($route, array('reportId' => $report->getId()));

        return $this->redirect($url);
    }

    /**
     * Redirect to the first unfinished step
     *
     * @Route("/next/{stepKey}", name="reports_build_next", defaults={"stepKey" = null})
     * @throws Exception
     */
    public function nextAction($stepKey)
    {
        $this->assertBuildEnabled();
        $this->assertValidBuiltReport();
        $this->checkReportPermission('UPDATE');

        $reportContainer = $this->getReportContainer();
        $report          = $reportContainer->getReport();
        $stepService     = $this->get('brown298.report_builder.build.step');

        // Find next step
        if ($report->isPublished()) {

            if (!$stepKey || !$stepService->isValidStepKey($stepKey)) {
                $error = 'You must pass a valid step parameter to this method if the report is published';
                throw new Exception($error);
            }

            $step = $stepService->getStepAfter($stepKey);

            if (!$step) {
                $url = $this->generateUrl('reports_view_index', array('reportId' => $report->getId()));

                return $this->redirect($url);
            }
        } else {
            $step = $stepService->getFirstUnfinishedStep();
        }

        $url = $this->generateUrl(
            'reports_build_step',
            array(
                'reportId' => $report->getId(),
                'stepKey'  => $step->getKey(),
            )
        );

        return $this->redirect($url);
    }

    /**
     * Redirect to the first step
     *
     * @Route("/first", name="reports_build_first")
     */
    public function firstAction()
    {
        $this->assertBuildEnabled();
        $this->assertValidBuiltReport();
        $this->checkReportPermission('UPDATE');

        $reportContainer = $this->getReportContainer();
        $report          = $reportContainer->getReport();
        $stepService     = $this->get('brown298.report_builder.build.step');
        $step            = $stepService->getFirstStep();

        $url = $this->generateUrl(
            'reports_build_step',
            array(
                'reportId' => $report->getId(),
                'stepKey'  => $step->getKey(),
            )
        );

        return $this->redirect($url);
    }

    /**
     * @param $stepService
     * @param $report
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function inaccessibleStep($stepService, $report)
    {
        $step_ = $stepService->getFirstUnfinishedStep();

        $route = 'reports_build_step';
        $params = array(
            'reportId' => $report->getId(),
            'stepKey' => $step_->getKey(),
        );
        $url = $this->generateUrl($route, $params);

        return $this->redirect($url);
    }

    /**
     * Display the form to finish a certain step
     *
     * @Route("/step/{stepKey}", name="reports_build_step")
     */
    public function stepAction(Request $request, $stepKey)
    {
        $this->assertBuildEnabled();
        $this->assertValidBuiltReport();
        $this->checkReportPermission('UPDATE');

        $sharingEnabled  = $this->isSharingEnabled();
        $security        = $this->getSecurity();
        $reportContainer = $this->getReportContainer();
        /** @var \Brown298\ReportBuilderBundle\Entity\BuiltReport $report */
        $report          = $reportContainer->getReport();
        $options         = $this->container->getParameter('brown298.report_builder.options');
        $classes         = $options['builder']['base_entities'];
        $treeMetadata    = $this->get('brown298.report_builder.tree_metadata');
        $class           = $report->getEntity();
        /** @var \Brown298\ReportBuilderBundle\Service\StepService $stepService  */
        $stepService     = $this->get('brown298.report_builder.build.step');
        $step            = $stepService->getCurrentStep();
        $stepKey         = $step->getKey();

        $treeMetadata->setSecurityService($security);

        // Check step
        if (!$stepService->isStepAccessible($stepKey)) {
            return $this->inaccessibleStep($stepService, $report);
        }

        // Build form
        $formBuilder = $stepService->createForm($treeMetadata, $security, $classes);
        $form        = $this->createForm($formBuilder, $report);

        // Table service
        if ($stepKey === 'filter') {
            $tableService = $this->get('brown298.report_builder.table');
            $tableService->setReportContainer($reportContainer);
            $dataTable  = $tableService->getDataTable();
            $dataTable->setFilterLimit(50);
            $dataTable->setPreview();
            if ($response = $dataTable->ProcessRequest($request)) {
                return $response;
            }
        }

        // Selection service
        if (in_array($stepKey, array('fields', 'filter', 'order'))) {
            /** @var \Brown298\ReportBuilderBundle\Service\PropertySelectionService $selectionService */
            $selectionService = $this->get('brown298.report_builder.build.selection');
            $selectionService->setReport($report);
            $selections = $selectionService->getSelections($stepKey);
        }


        // Post handling
        if ('POST' === $request->getMethod()) {

            // Pre bind event
            $dispatcher = $this->get('event_dispatcher');
            $em         = $this->get('brown298.report_builder.report.entity_manager');
            $event = new BuildStepEvent($report, $step, $em, $security, $class);
            $dispatcher->dispatch('brown298.report_builder.build.pre_bind', $event);

            // Request data to form and entity
            $form->submit($request);
            if ($form->isValid()) {

                $user     = $this->getUser();
                $userName = ($user != null) ? $user->getUsername(): 'anon';
                $report->setUpdatedBy($userName);

                // Pre persist event
                $event = new BuildStepEvent($report, $step, $em, $security, $class);
                $dispatcher->dispatch('brown298.report_builder.build.persist', $event);

                // Set report's finished status
                $isFinished = $stepService->isFinished();
                $report->setFinished($isFinished);

                // Save
                $em->persist($report);
                $em->flush();

                // Redirect
                $formName = $form->getName();
                $formData = $request->request->get($formName);

                $route = 'reports_build_step';
                $params = array('reportId' => $report->getId());

                if (isset($formData['redirect'])) {
                    $route = $formData['redirect'];
                }
                if (strpos($route, 'reports_build_') !== false) {
                    $params['stepKey'] = $stepKey;
                }

                return $this->redirect($this->generateUrl($route, $params));
            }
        }

        // Render
        $form            = $form->createView();
        $templateService = $this->get('brown298.report_builder.template.step');

        $params = compact('form', 'report', 'treeMetadata', 'dataTable', 'step', 'selections', 'sharingEnabled');
        return $this->render($templateService->getTemplateByStep($stepKey), $params);
    }
}