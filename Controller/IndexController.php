<?php
namespace Brown298\ReportBuilderBundle\Controller;

use Brown298\ReportBuilderBundle\DataTables\BuiltReportListDataTable;
use Brown298\ReportBuilderBundle\DataTables\SystemReportListDataTable;
use Brown298\ReportBuilderBundle\DataTables\SharedReportListDataTable;
use Doctrine\Common\Collections\ArrayCollection;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Filter\FilterTypeFactory;
use Brown298\ReportBuilderBundle\Filter\FilterTypeMap;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * IndexController
 *
 * @Route("/reports")
 */
class IndexController extends AbstractController
{
    /**
     * List of personal custom reports, shared custom reports, and system
     * reports. If building is disabled, only system reports are shown.
     *
     * @Route("/index/{tableName}", defaults={"tableName" = null}, name="reports_index")
     */
    public function indexAction(Request $request, $tableName = null)
    {
        $includeDeleted  = false;
        $args            = array( 'buildEnabled'  => $this->isBuildEnabled());
        $templateService = $this->get('brown298.report_builder.template.step');

        $this->checkPermission('VIEW');

        // system report table
        if ($this->isSystemEnabled() && $this->hasReportPermission('VIEW_SYSTEM')) {
            $args['systemReportList'] = $this->getSystemReportTable($includeDeleted);
            if ($tableName == "systemReportList" && $response = $args['systemReportList']->ProcessRequest($request)) {
                return $response;
            }
        }

        if ($args['buildEnabled'] && $this->hasReportPermission('VIEW_BUILT')) {
            // owned report table
            $args['builtReportList'] = $this->getOwnedReportTable($includeDeleted);
            if ($tableName == "builtReportList" && $response = $args['builtReportList']->ProcessRequest($request)) {
                return $response;
            }

            if ($this->isSharingEnabled() && $this->hasReportPermission('VIEW_SHARED')){
                // shared report table
                $args['sharedReportList'] = $this->getSharedReportTable($includeDeleted);
                if ($tableName == "sharedReportList" && $response = $args['sharedReportList']->ProcessRequest($request)) {
                    return $response;
                }
            }
        }

        return $this->render($templateService->getIndexTemplate(), $args);
    }

    /**
     * Create a new report object and pass it to the build interface
     *
     * @Route("/create", name="reports_create")
     */
    public function createAction()
    {
        $this->assertBuildEnabled();
        $this->checkReportPermission('CREATE');

        $report    = new BuiltReport();
        $em        = $this->get('doctrine.orm.entity_manager');
        
        $user     = $this->getUser();
        $userName = ($user != null) ? $user->getUsername(): 'anon';
        $report->setCreatedBy($userName);

        $em->persist($report);
        $em->flush($report);

        $id  = $report->getId();
        $url = $this->generateUrl('reports_build_index', array('reportId' => $id));

        return $this->redirect($url);
    }

    /**
     * deleteAction
     *
     * @Route("/{reportId}/delete", name="reports_delete")
     */
    public function deleteAction()
    {
        $this->checkReportPermission('DELETE');
        return $this->deleteReport(true);
    }

    /**
     * undeleteAction
     *
     * @Route("/{reportId}/undelete", name="reports_undelete")
     */
    public function undeleteAction()
    {
        $this->checkReportPermission('UNDELETE');

        return $this->deleteReport(false);
    }

    /**
     * filterFormAction
     *
     * @Route("/filterform", name="reports_filterform")
     */
    public function filterFormAction(Request $request)
    {
        $security     = $this->getSecurity();
        $treeMetadata = $this->get('brown298.report_builder.tree_metadata');
        $treeMetadata->setSecurityService($security);

        $class = $request->request->get('class');
        $items = $request->request->get('items');

        $data = array();
        $data['items'] = array();

        if (!empty($items)) {
            foreach ($items as $item) {

                $idx = $item['idx'];
                $path = $item['path'];
                $func = $item['func'];

                $response = $this->getFilterForm($class, $path, $func);
                $html = $response->getContent();

                $item = array();
                $item['idx'] = (int)$idx;
                $item['html'] = $html;

                $data['items'][] = $item;
            }
        }

        $data['success'] = true;

        return new JsonResponse($data);
    }

    /**
     * deleteReport
     *
     * @param $delete
     * @return null
     */
    private function deleteReport($delete)
    {
        $report = $this->get('brown298.report_builder.report')->getReport();

        $report->setDeleted($delete);

        $em = $this->get('brown298.report_builder.report.entity_manager');
        $em->persist($report);
        $em->flush();

        $url = $this->generateUrl('reports_index');

        return $this->redirect($url);
    }

    /**
     * getFilterForm
     *
     * @param string $class
     * @param string $path
     * @param string $func
     * @return Response
     */
    private function getFilterForm($class, $path, $func)
    {
        $runtimeFilters = array();
        if ($func === BuiltReport::FUNCTION_COUNT) {
            $keys        = FilterTypeMap::getKeysForCount();
            $filterTypes = FilterTypeFactory::create($keys);

        } else {
            $treeMeta  = $this->get('brown298.report_builder.tree_metadata');
            try {
                $filterTypes    = $treeMeta->getFilterTypes($class, $path, $func);
                $runtimeFilters = $treeMeta->getRuntimeTypes($class, $path, $func);
            } catch (\RuntimeException $ex) {
                return $this->getFilterFormError($ex->getMessage());
            }
        }

        $templateService = $this->get('brown298.report_builder.template.step');
        return $this->render($templateService->getFilterFormTemplate(), array('filterTypes' => $filterTypes, 'runtimeFilters' => $runtimeFilters));
    }

    /**
     * getFilterFormError
     *
     * @param string $message
     * @return Response
     */
    private function getFilterFormError($message)
    {
        $templateService = $this->get('brown298.report_builder.template.step');
        return $this->render($templateService->getErroSpanTemplate, array('message'=>$message));
    }

    /**
     * @param $includeDeleted
     * @return BuiltReportListDataTable
     */
    protected function getOwnedReportTable($includeDeleted)
    {
        $user      = $this->getUser();
        $userName  = ($user != null) ? $user->getUsername(): 'anon';
        $em        = $this->get('brown298.report_builder.report.entity_manager');
        $container = $this->get('service_container');
        $dataTable = new BuiltReportListDataTable($includeDeleted, $userName);
        $dataTable->setEm($em);
        $dataTable->setContainer($container);
        return $dataTable;
    }

    /**
     * @param $includeDeleted
     * @return BuiltReportListDataTable
     */
    protected function getSystemReportTable($includeDeleted)
    {
        $em        = $this->get('brown298.report_builder.report.entity_manager');
        $container = $this->get('service_container');
        $dataTable = new SystemReportListDataTable($includeDeleted);
        $dataTable->setEm($em);
        $dataTable->setContainer($container);
        return $dataTable;
    }

    /**
     * @param $includeDeleted
     * @return BuiltReportListDataTable
     */
    protected function getSharedReportTable($includeDeleted)
    {
        $user          = $this->getUser();
        $userName      = ($user != null) ? $user->getUsername(): 'anon';
        $em            = $this->get('brown298.report_builder.report.entity_manager');
        $container     = $this->get('service_container');
        $security      = $this->getSecurity();
        $canEditOthers = $security->getCanEditOthers();
        $dataTable = new SharedReportListDataTable($userName, $canEditOthers, $includeDeleted);
        $dataTable->setEm($em);
        $dataTable->setContainer($container);
        return $dataTable;
    }
}