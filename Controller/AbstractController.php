<?php
namespace Brown298\ReportBuilderBundle\Controller;

use Brown298\ReportBuilderBundle\Entity\Report;
use Brown298\ReportBuilderBundle\Service\ReportContainer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class AbstractController
 * @package Brown298\ReportBuilderBundle\Controller
 */
abstract class AbstractController extends BaseController
{
    /**
     * @var AbstractReportContainer
     */
    protected $reportContainer;

    /**
     * getReportContainer
     *
     * @return ReportContainer
     */
    protected function getReportContainer()
    {
        if (!$this->reportContainer) {
            /** @var \Brown298\ReportBuilderBundle\Service\ReportService $reportService */
            $reportService = $this->get('brown298.report_builder.report');
            $report        = $reportService->getReport();
            if ($report == null) {
                return null;
            }
            $this->reportContainer = $this->createReportContainer($report);
        }

        return $this->reportContainer;
    }

    /**
     * createReportContainer
     *
     * @param \Brown298\ReportBuilderBundle\Entity\Report $report
     * @internal param $Report
     * @return ReportContainer
     */
    protected function createReportContainer(Report $report)
    {
        if ($report->isBuilt()) {
            $em           = $this->get('brown298.report_builder.report.target_entity_manager');
            $treeMetadata = $this->get('brown298.report_builder.tree_metadata');
            $formFactory  = $this->get('form.factory');

            $treeMetadata->setSecurityService($this->getSecurity());

            $reportContainer = new ReportContainer\BuiltReportContainer($formFactory, $report, $em, $treeMetadata);

            return $reportContainer;
        }

        $container = $this->get('service_container');

        return new ReportContainer\StaticReportContainer($report, $container);
    }

    /**
     * assertValidReport
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return null
     */
    protected function assertValidReport()
    {
        $reportContainer = $this->getReportContainer();
        $report = $reportContainer->getReport();

        if ($report->isBuilt() && !$report->isValid()) {
            throw $this->createNotFoundException('Invalid report');
        }
    }

    /**
     * getSecurity
     *
     * @return \Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface
     */
    protected function getSecurity()
    {
        return  $this->container->get('brown298.report_builder.security');
    }

    /**
     * @param $permission
     * @return mixed
     */
    protected function hasReportPermission($permission)
    {
        $reportContainer = $this->getReportContainer();
        $security        = $this->getSecurity();

        if ($reportContainer != null) {
            $security->setReportContainer($reportContainer);
            $security->checkReportClassPermissions();
        }

        return $security->hasPermissions($permission);
    }

    /**
     * @param $permission
     * @return mixed
     */
    public function hasPermission($permission)
    {
        $security = $this->getSecurity();

        return $security->hasPermissions($permission);
    }

    /**
     * checkReportPermission
     *
     * checks that the user has permission to perform the current actions
     *
     * @param $permission
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    protected function checkReportPermission($permission)
    {
        if (!$this->hasReportPermission($permission)) {
            throw new AccessDeniedException($permission);
        }
    }

    /**
     * @param $permission
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    protected function checkPermission($permission)
    {
        if (!$this->hasPermission($permission)) {
            throw new AccessDeniedException($permission);
        }
    }

    /**
     * assertValidBuiltReport
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return null
     */
    protected function assertValidBuiltReport()
    {
        $reportContainer = $this->getReportContainer();
        $report          = $reportContainer->getReport();

        if (!$report->isBuilt()) {
            throw $this->createNotFoundException('Invalid report');
        }

        $this->assertValidReport();
    }

    /**
     * assertBuildEnabled
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return null
     */
    protected function assertBuildEnabled()
    {
        if (!$this->isBuildEnabled()) {
            throw $this->createNotFoundException('Report builder not enabled');
        }
    }

    /**
     * isBuildEnabled
     *
     * @return boolean
     */
    protected function isBuildEnabled()
    {
        $config = $this->container->getParameter('brown298.report_builder.options');
        return $config['builder']['enabled'];
    }

    /**
     * isBuildEnabled
     *
     * @return boolean
     */
    protected function isSharingEnabled()
    {
        $config = $this->container->getParameter('brown298.report_builder.options');
        return $config['builder']['shared_reports'];
    }

    /**
     * isBuildEnabled
     *
     * @return boolean
     */
    protected function isSystemEnabled()
    {
        $config = $this->container->getParameter('brown298.report_builder.options');
        return $config['builder']['system_reports'];
    }
}

