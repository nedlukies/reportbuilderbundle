<?php
namespace Brown298\ReportBuilderBundle\Validator\Constraints;

use Brown298\ReportBuilderBundle\Service\PropertySelectionService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class OnlySelectionsValidator
 * @package Brown298\ReportBuilderBundle\Validator\Constraints
 */
class OnlySelectionsValidator extends ConstraintValidator
{

    /**
     * @var PropertySelectionService
     */
    protected $selectionService;

    /**
     * Constructor
     *
     * @param  PropertySelectionService $selectionService
     */
    public function __construct(PropertySelectionService $selectionService)
    {
        $this->selectionService = $selectionService;
    }

    /**
     * isValid
     *
     * @param  BuiltReport $report
     * @param  Constraint  $constraint
     * @return null
     */
    public function validate($report, Constraint $constraint)
    {
        $this->selectionService->setReport($report);

        if (!$this->selectionService->canReportBeSelected()) {
            return;
        }

        $group = $this->context->getGroup();
        $stepKey = preg_replace('/_step$/', '', $group);
        $invalid = $this->selectionService->getInvalidReportAssets($stepKey);

        if (!$invalid) {
            return true;
        }

        $disallowed = array();
        foreach ($invalid as $asset) {

            $path = $asset->getPath();
            $func = $asset->getFunction();

            $format = $path && $func ? '%s (%s)' : '%s%s';
            $disallowed[] = sprintf($format, $path, $func);
        }

        $params = array('%disallowedList%' => implode(', ', $disallowed));
        $this->context->addViolation($constraint->message, $params, null);

        return false;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'only_selection';
    }
}