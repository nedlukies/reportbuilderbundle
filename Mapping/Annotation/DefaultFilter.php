<?php
namespace Brown298\ReportBuilderBundle\Mapping\Annotation;

use InvalidArgumentException;

/**
 * Class DefaultFilter
 * @package Brown298\ReportBuilderBundle\Mapping\Annotation
 * @Annotation
 * @Target({"CLASS"})
 */
class DefaultFilter
{
    /**
     * @var
     */
    protected $type;

    /**
     * @var
     */
    protected $field;

    /**
     * @var
     */
    protected $value;

    /**
     * Constructor
     *
     * @param $values
     * @return \Brown298\ReportBuilderBundle\Mapping\Annotation\DefaultFilter
     */
    public function __construct($values)
    {
        if (!isset($values['type']) ||!isset($values['field']) ||!isset($values['value']) ) {
            throw new InvalidArgumentException('you must specif a type, field and value for a default filter');
        }

        $this->type = $values['type'];
        $this->field = $values['field'];
        $this->value = $values['value'];
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }


}