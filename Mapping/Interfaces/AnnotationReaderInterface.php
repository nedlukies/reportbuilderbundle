<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;

/**
 * Interface AnnotationReaderInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
interface AnnotationReaderInterface
{
    /**
     * setTreeMeta
     *
     * @param TreeMetadata $treeMetaData
     * @return null
     */
    public function setTreeMetadata(TreeMetadata $treeMetaData);

    /**
     * getTreeMeta
     *
     * @return TreeMetadata
     */
    public function getTreeMetadata();

    /**
     * @param $annotationClass
     * @param $targetClass
     * @return mixed
     */
    public function findMetadata($annotationClass, $targetClass);

    /**
     * @param Reader $reader
     * @return mixed
     */
    public function setMetadataReader(Reader $reader);

} 