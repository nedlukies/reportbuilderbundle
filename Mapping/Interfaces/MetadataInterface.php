<?php
namespace Brown298\ReportBuilderBundle\Mapping\Interfaces;

use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;

/**
 * Interface MetadataInterface
 * @package Brown298\ReportBuilderBundle\Mapping\Interfaces
 */
Interface MetadataInterface
{
    /**
     * @param TreeMetadata $treeMetaData
     * @return mixed
     */
    public function setTreeMetadata(TreeMetadata $treeMetaData);

    /**
     * getTreeMeta
     *
     * @return TreeMetadata
     */
    public function getTreeMetadata();

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel();
}