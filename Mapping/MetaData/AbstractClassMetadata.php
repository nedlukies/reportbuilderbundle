<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Common\Util\Inflector;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Mapping\Exception\CreationException;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;

/**
 * Class AbstractClassMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
abstract class AbstractClassMetadata extends AbstractMetadata implements ClassMetadataInterface
{
    /**
     * @var \Doctrine\ORM\Mapping\ClassMetadataInfo
     */
    protected $doctrineClassMetadata;

    /**
     * @var array
     */
    protected $properties;

    /**
     * @var PropertyMetadataInterface|null
     */
    protected $parentPropertyMetadata;

    /**
     * @var PropertyMetadataFactory
     */
    protected $propertyMetadataFactory;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var \ReflectionClass
     */
    protected $reflectionObject;

    /**
     * @param ClassMetadataFactory $classMetadataFactory
     * @param PropertyMetadataFactory $propertyMetadataFactory
     * @param SecurityInterface $security
     * @param TreeMetadata $treeMetadata
     * @param PropertyMetadataInterface $parentPropertyMetadata
     */
    public function __construct(
        ClassMetadataFactory      $classMetadataFactory,
        PropertyMetadataFactory   $propertyMetadataFactory,
        SecurityInterface         $security,
        TreeMetadata              $treeMetadata           = null,
        PropertyMetadataInterface $parentPropertyMetadata = null
    ) {
        $this->parentPropertyMetadata  = $parentPropertyMetadata;
        $this->propertyMetadataFactory = $propertyMetadataFactory;

        parent::__construct($classMetadataFactory, $security, $treeMetadata);
    }

    /**
     * @param string $class
     * @return mixed|void
     * @throws CreationException
     */
    public function init($class)
    {
        if (!class_exists($class)) {
            $message = sprintf('Class %s does not exist', $class);
            throw new CreationException($message);
        }

        $this->class = $class;
        $ignore      = $this->findMetadata('Ignored', $class);

        if ($ignore) {
            $message = sprintf('Class %s is ignored', $this->class);
            throw new CreationException($message);
        }
    }

    /**
     * Get parent property
     *
     * @return PropertyMetadata
     */
    public function getParentPropertyMetadata()
    {
        return $this->parentPropertyMetadata;
    }

    /**
     * getPropertyMetadata
     *
     * @param string $name
     * @return PropertyMetadata
     */
    public function getPropertyMetadata($name)
    {
        $propertyMetadatas = $this->getPropertyMetadatas();

        foreach ($propertyMetadatas as $propertyMetadata) {
            if ($propertyMetadata->getName() === $name) {
                return $propertyMetadata;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * getReflection
     *
     * @param $class
     * @return \ReflectionClass
     */
    protected function getReflectionClass($class)
    {
        if (is_null($this->reflectionObject)) {
            $this->reflectionObject = new \ReflectionClass($class);
        }

        return $this->reflectionObject;
    }


    /**
     * findLabel
     *
     * @return string
     */
    protected function findLabel()
    {
        $translator = null;
        $treeMeta = $this->getTreeMetadata();
        if ($treeMeta) {
            $translator = $treeMeta->getTranslator();
        }

        // Get label from parent property
        $parentPropertyMeta = $this->getParentPropertyMetadata();
        if ($parentPropertyMeta) {
            $label = $parentPropertyMeta->getLabel();
            return $label;
        }

        // Parse label from object properties
        $label = $this->getClass();
        $label = Inflector::labelize($label);

        if ($translator) {
            $label = $translator->trans($label);
        }

        return $label;
    }

    /**
     * isValidPath
     *
     * @param mixed $path
     * @return bool
     */
    public function isValidPath($path)
    {
        try {
            $propertyMetadata = $this->getPropertyMetadataByPath($path);
            $valid = !is_null($propertyMetadata);
        } catch (\InvalidArgumentException $e) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * getNamespace
     *
     * @param string $class
     * @return string
     */
    public function getNamespace($class)
    {
        $reflectionObject = $this->getReflectionClass($class);

        return $reflectionObject->getNamespaceName();
    }


    /**
     * getPropertyMetaDatasForTree
     *
     * @return PropertyMetadata[]
     */
    public function getPropertyMetadatasForTree()
    {
        $propertyMetadatas = $this->getPropertyMetadatas();

        // Apply filters
        $propertyMetadatas = $this->applyPropertyMetadataFilterDeep($propertyMetadatas);
        $propertyMetadatas = $this->applyPropertyMetadataFilterInverse($propertyMetadatas);
        $propertyMetadatas = $this->applyPropertyMetadataFilterPrimaryKeys($propertyMetadatas);
        $propertyMetadatas = $this->applyPropertyMetadataFilterSecurity($propertyMetadatas);

        return $propertyMetadatas;
    }


    /**
     * getPropertyMetadatasForTreeAggregatable
     *
     * @return ReportPropertyMetadata[]
     */
    public function getPropertyMetadatasForTreeAggregatable()
    {
        $propertyMetadatas = $this->getPropertyMetadatasForTree();

        // Apply filters
        $propertyMetadatas = $this->applyPropertyMetadataFilterAggregatable($propertyMetadatas);

        return $propertyMetadatas;
    }

    /**
     * getPropertyMetadatasForTreeOnlyFields
     *
     * @param null $context
     * @return ReportPropertyMetadata[]
     */
    public function getPropertyMetadatasForTreeOnlyFields($context = null)
    {
        $propertyMetadatas = $this->getPropertyMetadatasForTree();

        // Apply filters
        $propertyMetadatas = $this->applyPropertyMetadataFilterFields($propertyMetadatas, $context['report']);

        return $propertyMetadatas;
    }


    /**
     * Do not include properties that are deeper than a certain level.
     *
     * @param PropertyMetadata[] $propertyMetadatas
     * @throws \Exception
     * @return PropertyMetadata[]
     */
    protected function applyPropertyMetadataFilterDeep($propertyMetadatas)
    {
        // Get depth
        $parentPropertyMetadata = $this->getParentPropertyMetadata();
        if ($parentPropertyMetadata) {
            $depth = $parentPropertyMetadata->getDepth();
        } else {
            $depth = 0;
        }

        // Depth limit
        $treeMetadata = $this->getTreeMetadata();

        if (!$treeMetadata) {
            throw new \Exception('TreeMetadata not set');
        }

        $depthLimit = $treeMetadata->getDepthLimit();

        // Is the next level too deep?
        $goDeeper = $depth + 1 < $depthLimit;

        // Filter
        foreach ($propertyMetadatas as $i => $propertyMeta) {
            if ($propertyMeta->hasAssociatedClass() && !$goDeeper) {
                unset($propertyMetadatas[$i]);
            }
        }

        $propertyMetadatas = array_values($propertyMetadatas);

        return $propertyMetadatas;
    }

    /**
     * getPropertyMetadatas
     *
     * @return PropertyMetadata[]
     */
    public function getPropertyMetadatas()
    {
        if (!is_null($this->properties)) {
            return $this->properties;
        }

        $class        = $this->getClass();
        $reflect      = $this->getReflectionClass($class);
        $reflectProps = $reflect->getProperties();

        foreach ($reflectProps as $reflectProp) {
            $name = $reflectProp->name;

            $property = $this->findProperty($name);
            if ($property) {
                $this->properties[] = $property;
            }
        }

        return $this->properties;
    }

    /**
     * Find property
     *
     * @param string $name
     * @return PropertyMetadataInterface
     */
    protected function findProperty($name)
    {
        $property = $this->propertyMetadataFactory->generate($this->classMetadataFactory, $this, $name);

        return $property;
    }

    /**
     * hasPropertyMetadatas
     *
     * @return bool
     */
    public function hasPropertyMetadatas()
    {
        $propertyMetas = $this->getPropertyMetadatas();

        return count($propertyMetas) > 0;
    }

    /**
     * getShortClass
     *
     * @param $class
     * @return string
     */
    public function getShortClass($class)
    {
        $short = $this->getTreeMetadata()->normalizeClassNameToShorthand($class);

        return $short;
    }

    /**
     * getPropertyMetaByPathPieces
     *
     * @param $pieces
     * @return PropertyMetadataInterface
     */
    protected function getPropertyMetadataByPathPieces($pieces)
    {
        if (count($pieces) === 1) {
            $name = reset($pieces);
            return $this->getPropertyMetadata($name);
        }

        $name         = array_shift($pieces);
        $propertyMeta = $this->getPropertyMetadata($name);

        if (!$propertyMeta) {
            return null;
        }

        $classMeta    = $propertyMeta->getAssociatedClassMetadata();
        $propertyMeta = $classMeta->getPropertyMetadataByPathPieces($pieces);

        return $propertyMeta;
    }

    /**
     * getPropertyMetaByPath
     *
     * @param $path
     * @throws \InvalidArgumentException
     * @return PropertyMetadataInterface
     */
    public function getPropertyMetadataByPath($path)
    {
        if (!is_string($path)) {
            throw new \InvalidArgumentException;
        }

        $pieces = explode('.', $path);
        $propertyMeta = $this->getPropertyMetadataByPathPieces($pieces);

        return $propertyMeta;
    }

    /**
     * getClassesInPath
     *
     * @param string $path
     * @return array
     */
    public function getClassesInPath($path)
    {
        $classes = array();

        $propertyMeta = $this->getPropertyMetadataByPath($path);

        while ($propertyMeta) {
            $classMeta    = $propertyMeta->getClassMetadata();
            $classes[]    = $classMeta->getClass();
            $propertyMeta = $classMeta->getParentPropertyMetadata();
        }

        $classes = array_reverse($classes);

        return $classes;
    }

    /**
     * getIdentifierPropertyMetadata
     *
     * @return string|null
     */
    public function getIdentifierPropertyMetadata()
    {
        $idMappings = $this->classMetadataFactory->getDoctrineReader()->findIdMappings($this->getClass());
        if (!empty($idMappings)) {
            return array_pop($idMappings);
        }

        return null;
    }

    /**
     * getTopClassMeta
     *
     * @return ClassMetadata
     */
    public function getTopClassMetaData()
    {
        $propertyMeta = $this->getParentPropertyMetadata();

        // Stop
        if (!$propertyMeta) {
            return $this;
        }

        return $propertyMeta->getClassMetadata()->getTopClassMetadata();
    }

    /**
     * Do not include properties that are primary keys
     *
     * @param PropertyMetadata[] $propertyMetadatas
     * @return PropertyMetadata[]
     */
    protected function applyPropertyMetadataFilterPrimaryKeys($propertyMetadatas)
    {
        $idMappings = $this->classMetadataFactory->getDoctrineReader()->findIdMappings($this->getClass());

        foreach ($propertyMetadatas as $i => $propertyMetaData) {
            foreach($idMappings as $idMap) {
                if ($propertyMetaData->getName() == $idMap) {
                    $showPrimaryKey = $this->findMetadata('ShowPrimaryKey', $this->getClass());

                    if (!$showPrimaryKey || (isset($showPrimaryKey) && $showPrimaryKey->getValue() == false)) {
                        unset($propertyMetadatas[$i]);
                    }
                }
            }
        }
        $propertyMetadatas = array_values($propertyMetadatas);

        return $propertyMetadatas;
    }


    /**
     * If we're using SQL Server, do not include any properties that cannot be
     * aggregatable
     *
     * @param  PropertyMetadata[] $propertyMetadatas
     * @throws \Exception
     * @return PropertyMetadata[]
     */
    protected function applyPropertyMetadataFilterAggregatable($propertyMetadatas)
    {
        $treeMetadata = $this->getTreeMetadata();

        if (!$treeMetadata) {
            throw new \Exception('TreeMetadata not set');
        }

        $em = $treeMetadata->getEm();
        $platform = $em->getConnection()->getDriver()->getDatabasePlatform()->getName();

        if ($platform !== 'mssql') {
            return $propertyMetadatas;
        }

        foreach ($propertyMetadatas as $i => $propertyMeta) {
            if (!$propertyMeta->isAggregatable()) {
                unset($propertyMetadatas[$i]);
            }
        }

        $propertyMetadatas = array_values($propertyMetadatas);

        return $propertyMetadatas;
    }


    /**
     * Do not include the second side of a bidirectional association.
     *
     * @param PropertyMetadata[] $propertyMetaDatas
     * @return PropertyMetadata[]
     */
    protected function applyPropertyMetadataFilterInverse($propertyMetaDatas)
    {
        $parentPropertyMeta = $this->getParentPropertyMetadata();

        // If there's no parent, there's nothing to filter out
        if (!$parentPropertyMeta) {
            return $propertyMetaDatas;
        }

        // Get parent class
        $parentClassMeta = $parentPropertyMeta->getClassMetadata();
        $parentClass = $parentClassMeta->getClass();

        // Filter
        foreach ($propertyMetaDatas as $i => $propertyMeta) {
            if (!$propertyMeta->hasAssociatedClass()) {
                continue;
            }

            $classMeta = $propertyMeta->getAssociatedClassMetadata();
            $class = $classMeta->getClass();

            if ($class === $parentClass) {
                unset($propertyMetaDatas[$i]);
            }
        }
        $propertyMetaDatas = array_values($propertyMetaDatas);

        return $propertyMetaDatas;
    }

    /**
     * getRoles
     *
     * @return array
     */
    public function getRoles()
    {
        // Check report bundle annotation
        $secure = $this->findMetadata('Secure', $this->getClass());

        if ($secure) {
            return $secure->getRoles();
        }

        return array();
    }


    /**
     * getSelections
     *
     * @param  string $propertyRetrievalMethod
     * @param null $context
     * @return PropertySelection[]
     * @throws InvalidArgumentException
     */
    public function getSelections($propertyRetrievalMethod = null, $context = null)
    {
        if ($propertyRetrievalMethod === null) {
            $propertyRetrievalMethod = 'getPropertyMetadatasForTree';
        }

        if (!is_callable(array($this, $propertyRetrievalMethod))) {
            $message = 'Invalid method "%s::%s"';
            $message = sprintf($message, get_class($this), $propertyRetrievalMethod);
            throw new InvalidArgumentException($message);
        }

        $propertyMetadatas = $this->$propertyRetrievalMethod($context);
        $selections = array();

        /** @var PropertyMetadata $propertyMetadata */
        foreach ($propertyMetadatas as $propertyMetadata) {
            /** @var PropertySelection $selection */
            $selection = $propertyMetadata->createSelection();

            if ($propertyMetadata->hasAssociatedClass()) {

                $classMetadata = $propertyMetadata->getAssociatedClassMetadata();
                $children      = $classMetadata->getSelections($propertyRetrievalMethod, $context);
                $selection->setChildren($children);

                // If there are no children, get rid of parent
                if (!$children) {
                    continue;
                }

                // If there's only one descendant, merge it into parent
                $descendant = $selection->getOneDescendant();
                if ($descendant) {
                    $selection->setPath($descendant->getPath());
                    $selection->setChildren(array());
                }
            }

            $selections[] = $selection;
        }

        return $selections;
    }

    /**
     * getPropertyMetadatasForTreeOnlyFields
     *
     * @param $propertyMetadatas
     * @param \Brown298\ReportBuilderBundle\Entity\BuiltReport $report
     * @return PropertyMetadata[]
     */
    public function applyPropertyMetadataFilterFields($propertyMetadatas, BuiltReport $report)
    {
        $return = array();
        $fields = $report->getFields();

        /** @var PropertyMetadataInterface $propertyMetadata */
        foreach ($propertyMetadatas as $propertyMetadata) {

            if ($propertyMetadata->hasAssociatedClass()) {
                $return[] = $propertyMetadata;
                continue;
            }

            $propertyMetaPath = $propertyMetadata->getPath();
            /** @var Field $field */
            foreach ($fields as $field) {

                if ($field->isCountField()) {
                    continue;
                }

                if ($field->getPath() !== $propertyMetaPath) {
                    continue;
                }

                $return[] = $propertyMetadata;
            }
        }

        return $return;
    }


    /**
     * Do not include any properties related to classes that the current User
     * is not allowed to view.
     *
     * @param  PropertyMetadata[] $propertyMetadatas
     * @throws \Exception
     * @return PropertyMetadata[]
     */
    protected function applyPropertyMetadataFilterSecurity($propertyMetadatas)
    {
        $treeMetadata = $this->getTreeMetadata();

        if (!$treeMetadata) {
            throw new \Exception('ReportTreeMeta not set');
        }

        $security = $treeMetadata->getSecurityService();

        if (!$security) {
            throw new \Exception('SecurityService not set on ReportTreeMeta object');
        }

        /** @var PropertyMetadata $propertyMetadata */
        foreach ($propertyMetadatas as $i => $propertyMetadata) {

            // remove secured fields
            if ($propertyMetadata->isSecured()) {
                unset($propertyMetadatas[$i]);
            }

            $classMetadata = $propertyMetadata->getAssociatedClassMetadata();
            if (!$classMetadata) {
                continue;
            }

            $class = $classMetadata->getClass();

            if (!$security->isViewingAllowed($class)) {
                unset($propertyMetadatas[$i]);
            }
        }

        $propertyMetadatas = array_values($propertyMetadatas);

        return $propertyMetadatas;
    }

    /**
     * getDoctrineClassMetadata
     *
     * @param $class
     * @throws \Exception
     * @return \Doctrine\ORM\Mapping\ClassMetadataInfo
     */
    public function getDoctrineClassMetadata($class)
    {
        if ($this->doctrineClassMetadata) {
            return $this->doctrineClassMetadata;
        }

        $treeMeta = $this->getTreeMetadata();
        if (!$treeMeta) {
            throw new \Exception('TreeMetadata not set');
        }

        $metadata = $treeMeta->findDoctrineClassMetadata($class);
        $this->doctrineClassMetadata = $metadata;

        return $metadata;
    }


    /**
     * findJoinType
     *
     * @return null
     */
    public function findJoinType()
    {
        $joinType = $this->findMetadata('JoinType', $this->getClass());
        if ($joinType) {
            return $joinType->getJoinType();
        }

        return null;
    }

    /**
     * findDefaultFilter
     */
    public function findDefaultFilter()
    {
        $defaultFilter = $this->findMetadata('DefaultFilter', $this->getClass());

        if ($defaultFilter) {
            $filter = new \Brown298\ReportBuilderBundle\Entity\Filter();
            $filter->setPath($defaultFilter->getField());
            $filter->setType($defaultFilter->getType());

            $value = $defaultFilter->getValue();
            if ($defaultFilter->getType() == 'DateTime') {
                $date = new \DateTime($value);
                $value = $date->format(\DateTime::ISO8601);
            }

            $filter->setValue($value);
            return array($filter);
        }

        return array();
    }

    /**
     * @param $metadataClass
     * @param $targetClass
     * @return mixed
     */
    abstract public function findMetadata($metadataClass, $targetClass);
}