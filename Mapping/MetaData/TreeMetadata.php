<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\TranslatableInterface;
use Exception;
use ErrorException;
use InvalidArgumentException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TreeMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
class TreeMetadata implements TranslatableInterface
{
    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var integer
     */
    protected $depthLimit = 1;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ClassMetadataFactory
     */
    protected $classMetadataFactory;

    /**
     * @var SecurityService
     */
    private $security;

    /**
     * setSecurityService
     *
     * @param SecurityInterface $security
     * @return null
     */
    public function setSecurityService(SecurityInterface $security)
    {
        $this->security = $security;
    }

    /**
     * getSecurityService
     *
     * @return SecurityService
     */
    public function getSecurityService()
    {
        return $this->security;
    }

    /**
     * getRoles
     *
     * @param string $class
     * @throws \InvalidArgumentException
     * @return array
     */
    public function getRoles($class)
    {
        $classMeta = $this->getClassMetadata($class);

        if (!$classMeta) {
            $message = sprintf('Invalid class "%s"', $class);
            throw new InvalidArgumentException($message);
        }

        return $classMeta->getRoles();
    }

    /**
     * setTranslator
     *
     * @param TranslatorInterface $translator
     * @return null
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * getTranslator
     *
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * setDepthLimit
     *
     * @param integer $limit
     * @throws \InvalidArgumentException
     * @return null
     */
    public function setDepthLimit($limit)
    {
        if (!is_int($limit) || $limit < 1) {
            $message = 'Must be an integer greater then zero';
            throw new InvalidArgumentException($message);
        }

        $this->depthLimit = $limit;
    }

    /**
     * getDepthLimit
     *
     * @return integer
     */
    public function getDepthLimit()
    {
        return $this->depthLimit;
    }

    /**
     * setEm
     *
     * @param EntityManager $em
     * @return null
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * getEm
     *
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * hasEm
     *
     * @return bool
     */
    public function hasEm()
    {
        return (bool) $this->em;
    }

    /**
     * getClassMeta
     *
     * @param  string $class "FooBundle:Thing"; "FooBundle\Entity\Thing"
     * @return ClassMetadata
     */
    public function getClassMetadata($class)
    {
        $class = $this->normalizeClassNameToFull($class);

        if (!$class) {
            return null;
        }

        $this->classMetadataFactory->setSecurityService($this->getSecurityService());
        return $this->classMetadataFactory->generate($class, $this);
    }

    /**
     * isClassValid
     *
     * @param string $class
     * @return boolean
     */
    public function isClassValid($class)
    {
        $classMeta = $this->getClassMetadata($class);

        return $classMeta !== null;
    }

    /**
     * findDoctrineClassMetadata
     *
     * @param  string $class
     * @throws \ErrorException|\Exception
     * @throws \Exception
     * @return \Doctrine\ORM\Mapping\ClassMetadataInfo
     */
    public function findDoctrineClassMetadata($class)
    {
        if (!$this->hasEm()) {
            throw new Exception('Entity manager not set');
        }

        $em = $this->getEm();

        try {
            $metadata = $em->getClassMetadata($class);
        } catch (ErrorException $e) {
            if (strpos($e->getMessage(), 'does not exist') !== false) {
                return null;
            }

            throw $e;
        }

        return $metadata;
    }

    /**
     * Normalize class name from "FooBundle:Thing" to
     * "FooBundle\Entity\Thing" if necessary
     *
     * @param  string $class
     * @return string
     */
    public function normalizeClassNameToFull($class)
    {
        if (!preg_match('/^[^:]+:[^:]+$/', $class)) {
            return $class;
        }

        $metadata = $this->findDoctrineClassMetadata($class);

        if (!$metadata) {
            return null;
        }

        $class = $metadata->getName();

        return $class;
    }

    /**
     * Normalize class name from "FooBundle\Entity\Thing" to
     * "FooBundle:Thing" if necessary
     *
     * @param  string $class
     * @return string
     */
    public function normalizeClassNameToShorthand($class)
    {
        if (!preg_match('/^(\w+)\\\\(\w+)Bundle\\\\Entity\\\\(\w+)$/', $class, $matches)) {
            return $class;
        }

        $class = $matches[1].$matches[2].'Bundle:'.$matches[3];

        return $class;
    }

    /**
     * getFilterTypes
     *
     * passes filter types from class/property metadata
     *
     * @param $class
     * @param $path
     * @param $func
     * @return array
     */
    public function getFilterTypes($class, $path, $func)
    {
        $classMeta = $this->getClassMetadata($class);

        if (!$classMeta) {
            $message = 'Class does not exist';
            throw new \RuntimeException($message);
        }

        if (!$classMeta->isValidPath($path)) {
            $message = 'Invalid path';
            throw new \RuntimeException($message);
        }

        $propertyMeta = $classMeta->getPropertyMetadataByPath($path, $func);

        return $propertyMeta->getFilterTypes();
    }

    /**
     * getFilterTypes
     *
     * passes filter types from class/property metadata
     *
     * @param $class
     * @param $path
     * @param $func
     * @return array
     */
    public function getRuntimeTypes($class, $path, $func)
    {
        $classMeta = $this->getClassMetadata($class);

        if (!$classMeta) {
            $message = 'Class does not exist';
            throw new \RuntimeException($message);
        }

        if (!$classMeta->isValidPath($path)) {
            $message = 'Invalid path';
            throw new \RuntimeException($message);
        }

        $propertyMeta = $classMeta->getPropertyMetadataByPath($path, $func);

        return $propertyMeta->getRuntimeTypes();
    }

    /**
     * @return ClassMetadataFactory
     */
    public function getClassMetadataFactory()
    {
        return $this->classMetadataFactory;
    }

    /**
     * @param ClassMetadataFactory $classMetadataFactory
     */
    public function setClassMetadataFactory($classMetadataFactory)
    {
        $this->classMetadataFactory = $classMetadataFactory;
    }
} 