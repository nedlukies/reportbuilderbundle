<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation;

use Brown298\ReportBuilderBundle\Filter\RuntimeTypeFactory;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\AnnotationReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\AbstractPropertyMetadata;
use Doctrine\Common\Annotations\Reader;
use ReflectionProperty;
use Brown298\ReportBuilderBundle\Filter\FilterTypeFactory;
use Brown298\ReportBuilderBundle\Filter\FilterTypeMap;


/**
 * Class PropertyMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
class PropertyMetadata extends AbstractPropertyMetadata implements PropertyMetadataInterface, AnnotationReaderInterface
{
    /**
     * @var \ReflectionProperty
     */
    protected $reflectionProperty;

    /**
     * @var string
     */
    protected $bundleAnnotations = 'Brown298\ReportBuilderBundle\Mapping\Annotation\%s';

    /**
     * @var FileCacheReader
     */
    protected $annotationReader;

    /**
     * isIgnored
     *
     * @return boolean
     */
    public function isIgnored()
    {
        $ignores = $this->findAnnotations('Ignored', $this->getClass());
        $class   = $this->getTopClassMetadata()->getShortClass($this->getClass());
        $path    = $this->getPath();

        foreach ($ignores as $ignored) {
            if (!$ignored->hasFullPath()) {
                return true;
            }

            $ignoredEntity = $ignored->getEntity();
            $ignoredPaths = $ignored->getPaths();

            if ($ignoredEntity === $class && in_array($path, $ignoredPaths)) {
                return true;
            }
        }

        return false;
    }

    /**
     * isSecured
     *
     * @return boolean
     */
    public function isSecured()
    {
        $secured = $this->findAnnotations('Secure');
        foreach ($secured as $secure) {
            if (!$this->security->checkSecuredAnnotation($secure)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $translator
     * @return null
     */
    protected function getReportableLabel($translator)
    {
        $reportable = $this->findMetadata('Reportable', $this->getClass());
        if ($reportable && $reportable->hasLabel()) {
            $label = $reportable->getLabel();

            if ($translator) {
                $label = $translator->trans($label);
            }

            return $label;
        }
        return null;
    }

    /**
     * findRuntimeTypes
     */
    public function findRuntimeTypes()
    {
        $fieldMapping = $this->doctrineReader->findFieldMapping($this->getClass(), $this->getName());
        if ($fieldMapping) {
            $columnType = $fieldMapping['type'];
            $keys = FilterTypeMap::getKeysForColumn($columnType);

            $runtimeTypes = RuntimeTypeFactory::create($keys, 'runtime');
            if ($runtimeTypes) {
                return $runtimeTypes;
            }
        }

        return array();
    }

    /**
     * findFilterTypes
     *
     * @return array
     */
    public function findFilterTypes()
    {
        // Get from filterable
        $filterable = $this->findMetadata('Filter', $this->getClass());
        if ($filterable) {

            $keys = $filterable->getTypes();
            if ($keys) {

                $filterTypes = FilterTypeFactory::create($keys);
                if ($filterTypes) {
                    return $filterTypes;
                }
            }

            $columnType = $filterable->getColumnType();
            if ($columnType) {

                $keys = FilterTypeMap::getKeysForColumn($columnType);
                $filterTypes = FilterTypeFactory::create($keys);
                if ($filterTypes) {
                    return $filterTypes;
                }
            }
        }

        // Parse from simple map
        $fieldMapping = $this->doctrineReader->findFieldMapping($this->getClass(), $this->getName());
        if ($fieldMapping) {
            $columnType = $fieldMapping['type'];
            $keys = FilterTypeMap::getKeysForColumn($columnType);

            $filterTypes = FilterTypeFactory::create($keys);
            if ($filterTypes) {
                return $filterTypes;
            }
        }

        return array();
    }

    /**
     * getReflectionProperty
     *
     * @param $class
     * @param $name
     * @return \ReflectionProperty
     */
    protected function getReflectionProperty($class, $name)
    {
        if (is_null($this->reflectionProperty)) {
            $this->reflectionProperty = new ReflectionProperty($class, $name);
        }

        return $this->reflectionProperty;
    }

    /**
     * findAnnotations
     *
     * @param  string  $class
     * @param  string  $targetClass
     * @param  boolean $single Return first result
     * @return object
     */
    public function findAnnotations($class, $targetClass = null, $single = false)
    {
        $reflect         = $this->getReflectionProperty($targetClass, $this->getName());
        $annotationClass = $this->getAnnotationClass($class);
        $reader          = $this->getAnnotationReader();
        $annotations     = $reader->getPropertyAnnotations($reflect);
        $return          = array();

        foreach ($annotations as $annotation) {
            if (!$annotation instanceof $annotationClass) {
                continue;
            }

            if ($single) {
                return $annotation;
            }

            $return[] = $annotation;
        }

        if ($single) {
            return null;
        }

        return $return;
    }

    /**
     * findMetadata
     *
     * @param  string $class
     * @param null $targetClass
     * @return object
     */
    public function findMetadata($class, $targetClass)
    {
        $single = true;
        $annotation = $this->findAnnotations($class, $targetClass, $single);

        return $annotation;
    }

    /**
     * getAnnotationClass
     *
     * @param string $class
     * @return string
     */
    public function getAnnotationClass($class)
    {
        // If the full class name pattern for annotation classes is set as a
        // property, allow the short class name to be used as a parameter
        if ($this->bundleAnnotations && strpos($class, '\\') === false) {
            $class = sprintf($this->bundleAnnotations, $class);
        }

        return $class;
    }


    /**
     * getAnnotationReader
     *
     * @throws \Exception
     * @return FileCacheReader
     */
    public function getAnnotationReader()
    {
        return $this->annotationReader;
    }

    /**
     * @param Reader $reader
     * @return mixed|void
     */
    public function setMetadataReader(Reader $reader)
    {
        $this->annotationReader = $reader;
    }
}