<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\MetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;

/**
 * Class AbstractMetadata
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
Abstract class AbstractMetadata implements MetadataInterface
{
    /**
     * @var ClassMetadataFactory
     */
    protected $classMetadataFactory;

    /**
     * @var TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface
     */
    protected $security;

    /**
     * @var string
     */
    protected $label;

    /**
     * @param ClassMetadataFactory $classMetadataFactory
     * @param SecurityInterface $security
     * @param TreeMetadata $treeMetadata
     */
    public function __construct(
        ClassMetadataFactory $classMetadataFactory,
        SecurityInterface    $security,
        TreeMetadata         $treeMetadata = null
    ) {
        $this->classMetadataFactory = $classMetadataFactory;
        $this->security = $security;
        if ($treeMetadata) {
            $this->setTreeMetadata($treeMetadata);
        }
    }

    /**
     * setTreeMeta
     *
     * @param TreeMetadata $treeMetaData
     * @return null
     */
    public function setTreeMetadata(TreeMetadata $treeMetaData)
    {
        $this->treeMetadata = $treeMetaData;
    }

    /**
     * getTreeMeta
     *
     * @return TreeMetadata
     */
    public function getTreeMetadata()
    {
        return $this->treeMetadata;
    }


    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        if (!$this->label) {
            $this->label = $this->findLabel();
        }

        return $this->label;
    }
    /**
     * findLabel
     *
     * @return string
     */
    abstract protected function findLabel();
}