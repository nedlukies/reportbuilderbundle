<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\ClassMetadataInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class DoctrineReader
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
class DoctrineReader
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var array
     */
    protected $metadata = array();

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $targetClass
     * @return \Doctrine\Common\Persistence\Mapping\ClassMetadata
     */
    public function getClassMetaData($targetClass)
    {
        if (isset($this->metadata[$targetClass])) {
            $metaData = $this->metadata[$targetClass];
        } else {
            $metaData = $this->em->getMetadataFactory()->getMetadataFor($targetClass);
            $this->metadata[$targetClass] = $metaData;
        }
        return $metaData;
    }

    /**
     * @param $targetClass
     * @param $propertyName
     * @return bool
     */
    public function isORMProperty($targetClass, $propertyName)
    {
        $metaData = $this->getClassMetaData($targetClass);
        if (isset($metaData->fieldMappings[$propertyName]) || isset($metaData->associationMappings[$propertyName])) {
            return true;
        }
        return false;
    }

    /**
     * @param $targetClass
     * @param $propertyName
     * @return null
     */
    public function findFieldMapping($targetClass, $propertyName)
    {
        $metaData = $this->getClassMetaData($targetClass);
        if (isset($metaData->fieldMappings[$propertyName])) {
            return $metaData->fieldMappings[$propertyName];
        }
        return null;
    }

    public function findIdMappings($targetClass)
    {
        $metaData = $this->getClassMetaData($targetClass);
        $identifiers = $metaData->getIdentifier();
        return $identifiers;
    }

    /**
     * @param $targetClass
     * @param $propertyName
     * @param $parentClass
     * @return null|string
     */
    public function findAssociatedClass($targetClass, $propertyName, $parentClass)
    {
        $metaData = $this->getClassMetaData($targetClass);

        if (isset($metaData->associationMappings[$propertyName])) {
            $classMetadata =  $metaData->associationMappings[$propertyName];
            $class =  $classMetadata['targetEntity'];

            // Doctrine allows this property to contain the class's
            // shortname only if the class is in the same namespace
            if (strpos($class, '\\') === false) {
                $namespace = $this->getNamespace($parentClass);
                $class = $namespace.'\\'.$class;
            }

            return $class;
        }

        return null;
    }


    /**
     * getReflection
     *
     * @param $class
     * @return \ReflectionClass
     */
    protected function getReflectionClass($class)
    {
        $reflectionObject = new ReflectionClass($class);

        return $reflectionObject;
    }

    /**
     * getNamespace
     *
     * @param string $class
     * @return string
     */
    public function getNamespace($class)
    {
        $reflectionObject = $this->getReflectionClass($class);

        return $reflectionObject->getNamespaceName();
    }
}