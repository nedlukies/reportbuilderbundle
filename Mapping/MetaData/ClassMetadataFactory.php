<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\PropertyMetadataInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\AnnotationReaderInterface;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\YmlReaderInterface;
use Doctrine\Common\Annotations\Reader;

/**
 * Class ClassMetadataFactory
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation
 */
class ClassMetadataFactory
{
    /**
     * @var string
     */
    protected $classMetaClass = 'Brown298\ReportBuilderBundle\Mapping\MetaData\Annotation\ClassMetadata';

    /**
     * @var SecurityService
     */
    private $security;

    /**
     * @var
     */
    protected $metadataReader;

    /**
     * @var PropertyMetadataFactory
     */
    protected $propertyMetadataFactory;

    /**
     * @var DoctrineReader
     */
    protected $doctrineReader;

    /**
     * @param DoctrineReader $doctrineReader
     * @param PropertyMetadataFactory $propertyMetadataFactory
     * @param null $metadataReader
     * @param null $metadataClassName
     */
    public function __construct(DoctrineReader $doctrineReader, PropertyMetadataFactory $propertyMetadataFactory, $metadataReader = null, $metadataClassName = null)
    {
        if ($metadataClassName != null) {
            $this->classMetaClass = $metadataClassName;
        }
        $this->metadataReader          = $metadataReader;
        $this->propertyMetadataFactory = $propertyMetadataFactory;
        $this->doctrineReader          = $doctrineReader;
    }

    /**
     * @param $class
     * @param TreeMetadata $treeMetadata
     * @param PropertyMetadataInterface $parentPropertyMetadata
     * @return mixed
     */
    public function generate($class, TreeMetadata $treeMetadata, PropertyMetadataInterface $parentPropertyMetadata = null){
        $className = $this->classMetaClass;

        $classMetadata = new $className($this, $this->propertyMetadataFactory, $this->getSecurityService(), $treeMetadata, $parentPropertyMetadata);

        $classMetadata->setMetadataReader($this->getMetadataReader());

        $classMetadata->init($class);

        return $classMetadata;
    }

    /**
     * setSecurityService
     *
     * @param SecurityInterface $security
     * @return null
     */
    public function setSecurityService(SecurityInterface $security)
    {
        $this->security = $security;
    }

    /**
     * getSecurityService
     *
     * @return SecurityService
     */
    public function getSecurityService()
    {
        return $this->security;
    }

    /**
     * @return mixed
     */
    public function getMetadataReader()
    {
        return $this->metadataReader;
    }

    /**
     * @param Reader $metadataReader
     */
    public function setMetadataReader(Reader $metadataReader)
    {
        $this->metadataReader = $metadataReader;
    }

    /**
     * @return DoctrineReader
     */
    public function getDoctrineReader()
    {
        return $this->doctrineReader;
    }

}