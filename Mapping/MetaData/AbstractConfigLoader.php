<?php
namespace Brown298\ReportBuilderBundle\Mapping\MetaData;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractConfigLoader
 * @package Brown298\ReportBuilderBundle\Mapping\MetaData
 */
abstract class AbstractConfigLoader
{
    /**
     * @var string
     */
    protected $type = 'yml';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $config = array();

    /**
     * @var array
     */
    protected $configPathSearch = array();

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var \Symfony\Component\HttpKernel\Kernel;
     */
    private $kernel;

    public function __construct(ContainerInterface $container, array $configPathSearch)
    {
        $this->container        = $container;
        $this->configPathSearch = $configPathSearch;
        $this->kernel           = $container->get('kernel');
    }

    /**
     * buildYml
     */
    protected function buildConfig()
    {
        $paths       = array('Resources' . DIRECTORY_SEPARATOR . 'config');
        $directories = $this->getPossibleDirectories($paths);

        foreach ($directories as $directory) {
            foreach ($directory as $path) {
                foreach ($this->configPathSearch as $file) {
                    $currentPath = $path . DIRECTORY_SEPARATOR . $file . '.' . $this->type;
                    if (file_exists($currentPath)) {
                        switch($this->type) {
                            case 'yml':
                                $this->config = array_merge($this->config, $this->parseYml($currentPath));
                                break;
                            case 'xml':
                                $this->config = array_merge($this->config, $this->parseXml($currentPath));
                                break;
                        }

                    }
                }
            }
        }
    }

    /**
     * @return mixed
     */
    protected function getBundles()
    {
        return $this->container->getParameter('kernel.bundles');
    }

    /**
     * getBundleDirectories
     *
     * gets an array of the bundle directories to search
     *
     * @return array
     */
    protected function getBundleDirectories()
    {
        $directories = array();
        $bundles     = $this->getBundles();

        if (count($bundles) > 0) {
            foreach ($bundles as $name => $namespace) {
                $directories[$name] = $this->kernel->getBundle($name)->getPath();
            }
        }

        return $directories;
    }


    /**
     * @param $searchPath
     * @return array
     */
    protected function getPossibleDirectories(array $searchPath)
    {
        $directories = array();
        $bundleDirs  = $this->getBundleDirectories();

        foreach($bundleDirs as $bundle=>$directory) {
            foreach ($searchPath as $dir) {
                $path = $directory . DIRECTORY_SEPARATOR . $dir;

                if (is_dir($path)) {
                    $directories[$bundle][$dir] = $path;
                }
            }
        }

        return $directories;
    }

    /**
     * getYmlParser
     *
     * @return Parser
     */
    protected function getYmlParser()
    {
        if ($this->parser === null) {
            $this->parser = new Parser();
        }
        return $this->parser;
    }

    /**
     * @param $filePath
     * @return array
     */
    protected function parseYml($filePath)
    {
        $parser    = $this->getYmlParser();
        $contents  = $parser->parse(file_get_contents($filePath));
        $config    = array();
        if (is_array($contents)) {
            foreach($contents as $entityName => $entityConfig) {
                $config[$entityName] = array('type' => 'yml', 'file' => $filePath, 'contents' => $entityConfig);
            }
        }

        return $config;
    }

    /**
     * @param $filePath
     * @return array
     * @throws \RuntimeException
     */
    protected function parseXml($filePath)
    {
        $config = array();
        $parser = $this->getXml($filePath);

        if ($parser == false) {
            throw new \RuntimeException('Error loading config file:' . $filePath);
        }

        foreach($parser->children() as $table) {
            /** @todo parse xml */
        }

        return $config;
    }

    /**
     * @param $filePath
     * @return \SimpleXMLElement
     * @throws \RuntimeException
     */
    protected function getXml($filePath)
    {
        $parser = @simplexml_load_file($filePath);

        return $parser;
    }
}