<?php
namespace Brown298\ReportBuilderBundle\Steps;

/**
 * Class EntityStep
 * @package Brown298\ReportBuilderBundle\Steps
 */
class EntityStep extends AbstractStep
{
    /**
     * @var bool
     */
    protected $requiredForView = true;

    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        return !is_null($this->getReport()->getEntity());
    }

    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        return 'Base Element';
    }
}