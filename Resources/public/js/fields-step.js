var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * FieldStepInterface
 *
 * @param targetElement
 * @param config
 */
brown298.FieldsStepInterface = function(targetElement, config) {
    var self = this
    this.config = jQuery.extend({
        'treeFilterClass':  '.tree',
        'childFilterClass': 'ul.items'
    }, config);

    /**
     * gets a tree object from the targeted element
     * @type {*}
     */
    this.getTree = brown298.memoize(function() {
        var treeElement = targetElement.find(self.config.treeFilterClass);
        var tree        =  new brown298.Tree(treeElement, self);

        tree.addCheckboxes(self.getTreeCallback);

        return tree;
    });

    /**
     * getTreeCallback
     *
     * @param event
     * @param field
     */
    this.getTreeCallback = function(event, field) {
        var list = self.getList();
        var path = field.getPath();
        var func = field.getFunc();

        if (field.getCheckbox().isChecked()) {
            list.addItem(path, func);
        } else {
            list.removeItemByPath(path, func);
        }
    };

    /**
     * getList
     *
     * @type {*}
     */
    this.getList = brown298.memoize(function() {
        var childrenItems = targetElement.find(self.config.childFilterClass);
        var list          = new brown298.FieldsStepList(childrenItems, self);

        return list;
    });

    /**
     * updateCheckboxes
     */
    this.updateCheckboxes = function() {
        var tree = self.getTree();
        tree.uncheckFields();

        var list  = self.getList();
        var items = list.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            var path  = items[i].getPath();
            var func  = items[i].getFunc();
            var field = tree.findField(path, func);

            if (field) {
                field.getCheckbox().setChecked(true);
            }
        }
    };

    /**
     * initialize
     */
    this.initialize = function() {
        self.getTree();
        self.getList();

        self.updateCheckboxes();

        targetElement.show();
    };

    this.initialize();
};