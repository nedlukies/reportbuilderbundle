var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * TreeCHeckbox
 */
brown298.TreeCheckbox = function(checkboxElement, callback, field) {
    var self = this;

    this.setChecked = function(checked) {
        checkboxElement.attr('checked', checked);
    };

    this.getField = function() {
        return field;
    };

    /**
     * returns if the checkbox is checked
     * @returns {*}
     */
    this.isChecked = function() {
        return checkboxElement.is(':checked');
    };

    /**
     * initialize
     */
    this.initialize = function() {
        checkboxElement.change(function(event) {
            callback(event, self.getField());
        });
    };

    this.initialize();
};