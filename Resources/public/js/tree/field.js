var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 *
 * @param targetElement
 * @param parentGroup
 * @param config
 * @constructor
 */
brown298.TreeField = function(targetElement, parentGroup, config) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);
    this.checkbox = null;

    this.getParentGroup = function() {
        return parentGroup;
    };

    this.hasGroup = function() {
        return self.getGroup() !== null;
    };

    this.getGroup = brown298.memoize(function() {
        var el_ = targetElement.children('.group');

        if (!el_.length) {
            return null;
        }

        var group = new brown298.TreeGroup(el_);
        group.setParentField(self);

        return group;
    });

    this.getLabel = brown298.memoize(function() {
        return targetElement.children('label');
    });

    this.getPropLabel = brown298.memoize(function() {
        return self.getLabel().find('span.prop-label');
    });

    this.getLabelPrefix =brown298. memoize(function() {
        return targetElement.attr('data-label-path-prefix');
    });

    this.getCheckbox = function() {
        return self.checkbox;
    };

    this.isExpanded = function() {
        if (!self.isParent()) {
            return;
        }

        return targetElement.hasClass('expanded');
    };

    this.expand = function(animate) {
        if (!self.isParent()) {
            return;
        }

        targetElement.addClass('expanded');
        self.getGroup().show(animate);
    };

    this.collapse = function(animate) {
        if (!self.isParent()) {
            return;
        }

        var callback = function() {
            self.getGroup().collapseFields(false);
        };

        targetElement.removeClass('expanded');
        self.getGroup().hide(animate, callback);
    };

    this.getPath = brown298.memoize(function() {
        return self.getAttrValue('data-path');
    });

    this.getFunc = brown298.memoize(function() {
        return self.getAttrValue('data-function');
    });

    this.getAttrValue = brown298.memoize(function(key) {
        var val = targetElement.attr(key);

        if (val === undefined || !val) {
            val = null;
        }

        return val;
    });

    this.isParent = brown298.memoize(function() {
        var group = self.getGroup();
        return (group !== null)
    });

    var getRootTree = brown298.memoize(function() {
        return self.getParentGroup().getRootTree();
    });

    this.addCheckbox = function(callback) {
        var cb = jQuery('<input />');
        cb.attr('type', 'checkbox');
        cb.hide();

        var control = self.getLabel().find('.control');
        control.append(cb);

        var checkbox = new brown298.TreeCheckbox(cb, callback, self);

        self.checkbox = checkbox;
    };

    this.addLink = function(callback) {
        var propLabel = self.getPropLabel();
        propLabel.addClass('linkified');

        propLabel.click(function(event) {
            callback(event, self);
        });
    };

    var addControl = function() {
        var control = jQuery('<span></span>');
        control.addClass('control');
        self.getLabel().prepend(control);
    };

    var applyClickEvent = function() {
        if (!self.isParent()) {
            return;
        }

        var callback = function(event) {
            if (self.isExpanded()) {
                self.collapse(true);
            } else {
                self.expand(true);
            }
        };

        var label = self.getLabel();
        label.click(callback);
    };

    this.initialize = function() {

        addControl();
        if (self.isParent()) {
            applyClickEvent();
        }

        self.getGroup();
    };

    this.initialize();
};