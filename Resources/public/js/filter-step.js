var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * FilterStepInterface
 *
 * @param targetElement
 * @param params
 * @constructor
 */
brown298.FilterStepInterface = function(targetElement, params, config) {
    var self = this;
    this.config = jQuery.extend({
        'treeFilterClass':  '.tree',
        'childFilterClass': 'ul.items',
        'resultFilter':     '.result',
        'loadingImageSource': '/bundles/brown298_report_builder_bundle/css/images/loading.gif'
    }, this.config);
    this.callbacks = {};

    /**
     *
     * @param event
     * @param filter
     */
    this.getTreeCallback = function(event, filter) {
        var list = self.getList();
        var path = filter.getPath();
        var func = filter.getFunc();

        list.addItem(path, func);
        self.showListPane();
    }

    /**
     *
     * @type {Function}
     */
    this.getTree = brown298.memoize(function() {
        var element = targetElement.find(self.config.treeFilterClass);
        var tree = new brown298.Tree(element, self);

        var callback = function() { self.showListPane(); };
        var title = 'Back to the filter list';
        tree.addControl('&larr; Back to list', callback, title);
        tree.addLinks(self.getTreeCallback);

        return tree;
    });

    var getControls = brown298.memoize(function() {
        var controls = jQuery('<p></p>');
        controls.addClass('list-controls');
        controls.hide();

        targetElement.parent().prev().before(controls);

        self.controls = controls;
        return self.controls;
    });

    var addControl = function(text, callback, title) {

        var link = brown298.getLink(text, callback);
        link.addClass('control');

        if (title) {
            link.attr('title', title);
        }

        var controls = getControls();
        controls.append(' ').append(link);

        return link;
    };

    this.getResult = brown298.memoize(function() {
        var element = targetElement.find(self.config.resultFilter);
        var result = new brown298.Result(element);
        return result;
    });

    this.getList = brown298.memoize(function() {
        var el_ = targetElement.find(self.config.childFilterClass);
        var list = new brown298.FilterStepList(el_, self);
        return list;
    });

    this.getListPane = function() {
        var el_ = targetElement.find(self.config.childFilterClass);
        var pane = targetElement.find('.list-pane');
        pane.show();

        var pane = targetElement.find('.list-pane');
        pane.hide();
    };

    this.getParams = function() {
        return params;
    };

    this.getListPane = brown298.memoize(function() {
        return targetElement.find('.list-pane');
    });

    this.getTreePane = brown298.memoize(function() {
        return targetElement.find('.tree-pane');
    });

    this.showListPane = function() {
        self.getTreePane().hide();
        self.getListPane().show();
    };

    this.showTreePane = function() {
        self.getListPane().hide();
        self.getTreePane().show();
    };

    /**
     * getCallbackRegistry
     *
     * @type {Function}
     */
    this.getCallbackRegistry = brown298.memoize(function() {
        return new brown298.FilterStepCallbackRegistry;
    });

    /**
     * preloads the loading image
     */
    this.preload = function() {
        var img = new Image();
        img.src = self.config.loadingImageSource;
    }

    /**
     * initialize
     */
    this.initialize = function() {
        self.preload();
        self.getTree();
        self.getList();

        self.showListPane();

        targetElement.show();
    };

    this.initialize();
};