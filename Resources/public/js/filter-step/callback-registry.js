var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * FilterStepCallbackRegistry
 * @param config
 * @constructor
 */
brown298.FilterStepCallbackRegistry = function(config) {
    var self = this;
    this.config = jQuery.extend({
    }, this.config);
    var callbacks = {};

    var getKey = function(type, action) {
        var key = type + '|' + action;
        return key;
    };

    self.set = function(type, action, callback) {
        var key = getKey(type, action);
        callbacks[key] = callback;
    };

    /**
     * get
     * @param type
     * @param action
     * @returns {*}
     */
    self.get = function(type, action) {
        var key = getKey(type, action);
        return callbacks[key];
    };
};