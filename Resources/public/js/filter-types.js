jQuery(function() {

    /**
     * baseRead
     *
     * @param type
     */
    var baseRead = function(type) {
        var item = type.getFilter().getItem();
        var el = type.getEl();
        var input = item.getInput('value');
        var ours = el.find('input.value');

        ours.val(input.val());

        /* watch filter types */
        ours = el.find('select.runtime-filter-types');
        input = item.getInput('runtimeFilterType');
        ours.val(input.val());

        var selectedType = ours.val();
        ours = el.find('input.runtime-filter-values');
        if (selectedType != 'Choice') {
            ours.hide();
        } else {
            ours.show();
        }

        /* watch filter choice values */
        input = item.getInput('runtimeFilterChoices');
        ours.val(input.val());

        ours = el.find('input.second_value');
        if (ours.length) {
            input = item.getInput('second_value');

            ours.val(input.val());
        }
    };

    /**
     * baseWrite
     *
     * @param type
     */
    var baseWrite = function(type) {
        var item = type.getFilter().getItem();
        var el = type.getEl();
        var input = item.getInput('value');
        var ours = el.find('input.value');

        input.val(ours.val());

        /* watch filter types */
        ours = el.find('select.runtime-filter-types option:selected');
        input = item.getInput('runtimeFilterType');
        input.val(ours.val());

        var selectedType = ours.val();
        ours = el.find('input.runtime-filter-values');
        if (selectedType != 'Choice') {
            ours.hide();
        } else {
            ours.show();
        }

        /* watch filter choice values */
        ours = el.find('input.runtime-filter-values');
        input = item.getInput('runtimeFilterChoices');
        input.val(ours.val());

        ours = el.find('input.second_value');
        if (ours.length) {
            input = item.getInput('second_value');

            input.val(ours.val());
        }
    };

    /**
     * baseEmpty
     * @param type
     */
    var baseEmpty = function(type) {
        var el = type.getEl();
        var ours = el.find('input.value');

        ours.val('');

        ours = el.find('input.second_value');
        if (ours.length) {
            ours.val('');
        }
    };

    /**
     *
     * @param type
     * @param callback
     */
    var baseApplyEvents = function(type, callback) {
        var el = type.getEl();
        var ours = el.find('input.value');

        ours.change(callback);
        ours.keyup(callback);

        /* watch filter types */
        ours = el.find('select.runtime-filter-types');
        ours.change(callback);
        ours.keyup(callback);

        /* watch filter values */
        ours = el.find('input.runtime-filter-values');
        ours.change(callback);
        ours.keyup(callback);

        ours = el.find('input.second_value');
        if (ours.length) {
            ours.change(callback);
            ours.keyup(callback);
        }


    };

    /**
     *
     * @param type
     */
    var baseFocus = function(type) {
        var el = type.getEl();
        var ours = el.find('input.value');

        ours.focus();
    };

    var oneOrTwoTextInputs = [
        'between_numbers',
        'contains',
        'does_not_contain',
        'ends_with',
        'equal_to',
        'equal_to_number',
        'greater_than',
        'greater_than_or_equal_to',
        'less_than',
        'less_than_or_equal_to',
        'not_between_numbers',
        'not_equal_to',
        'not_equal_to_number',
        'starts_with'
    ];

    for (var i = 0, length = oneOrTwoTextInputs.length; i < length; i++) {
        var key = oneOrTwoTextInputs[i];

        window.interf.getCallbackRegistry().set(key, 'read', baseRead);
        window.interf.getCallbackRegistry().set(key, 'write', baseWrite);
        window.interf.getCallbackRegistry().set(key, 'empty', baseEmpty);
        window.interf.getCallbackRegistry().set(key, 'applyEvents', baseApplyEvents);
        window.interf.getCallbackRegistry().set(key, 'focus', baseFocus);
    }

    var dateInit = function(type) {
        var el = type.getEl();
        var ours = el.find('input.value');

        var params = {
            changeMonth: true,
            changeYear: true,
            yearRange: '-100Y:+1Y',
            constrainInput: false
        };

        ours.datepicker(params);

        ours = el.find('input.second_value');
        if (ours.length) {
            ours.datepicker(params);
        }
    };

    var dateInputs = [
        'between_dates',
        'not_between_dates',
        'on_date',
        'on_or_after_date',
        'on_or_before_date',
        'between_datetimes',
        'not_between_datetimes',
        'on_datetime',
        'on_or_after_datetime',
        'on_or_before_datetime'
    ];

    for (var i = 0, length = dateInputs.length; i < length; i++) {
        var key = dateInputs[i];

        window.interf.getCallbackRegistry().set(key, 'init', dateInit);
        window.interf.getCallbackRegistry().set(key, 'read', baseRead);
        window.interf.getCallbackRegistry().set(key, 'write', baseWrite);
        window.interf.getCallbackRegistry().set(key, 'empty', baseEmpty);
        window.interf.getCallbackRegistry().set(key, 'applyEvents', baseApplyEvents);
        window.interf.getCallbackRegistry().set(key, 'focus', baseFocus);
    }
});