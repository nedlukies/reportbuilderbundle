var brown298 = (typeof brown298 != 'undefined') ? brown298 : {};

/**
 * FieldStepList
 *
 * @param targetElement
 * @param interf
 * @param config
 * @constructor
 */
brown298.FieldsStepList = function(targetElement, interf, config) {
    var self = this;
    this.config = jQuery.extend({
        'treeFilterClass':  '.tree',
        'childFilterClass': 'ul.items'
    }, config);

    /**
     * itemIdx
     * @type {null|int}
     */
    this.itemIdx = null;

    /**
     * controls
     * @type {null}
     */
    this.controls = null;

    /**
     * gets the items
     * @returns {Array}
     */
    this.getItems = function() {
        var items = [];

        targetElement.children('li').each(function() {

            var element = jQuery(this);
            var item    = element.data('fieldsStepItem');

            if (!item) {
                var idx = self.createItemIdx();

                try {
                    item = new brown298.FieldsStepItem(element, self, idx);
                } catch (e) {
                    element.remove();
                    return true;
                }

                element.data('fieldsStepItem', item);
            }

            items.push(item);
        });

        return items;
    };

    /**
     * getItem
     *
     * @param path
     * @param func
     * @returns {*}
     */
    this.getItem = function(path, func) {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            if (path == items[i].getPath() && func == items[i].getFunc()) {
                return items[i];
            }
        }

        return null;
    };

    /**
     * hasItem
     *
     * @param path
     * @param func
     * @returns {boolean}
     */
    this.hasItem = function(path, func) {
        return self.getItem(path, func) !== null;
    };

    /**
     * getInterface
     * @returns {*}
     */
    this.getInterface = function() {
        return interf;
    };

    /**
     * addItem
     *
     * @param path
     * @param func
     */
    this.addItem = function(path, func) {
        if (self.hasItem(path, func)) {
            return;
        }

        var html = targetElement.attr('data-prototype');
        var idx  = self.createItemIdx();
        html     = html.replace(/__name__/g, idx);

        var li = jQuery('<li></li>');
        li.append(html);

        var item = new brown298.FieldsStepItem(li, self, idx, path, func);

        li.data('fieldsStepItem', item);

        targetElement.append(li);

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
        self.getInterface().updateCheckboxes();
    };

    /**
     * hasCountItem
     * @returns {boolean}
     */
    this.hasCountItem = function() {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            if (items[i].isCountField()) {
                return true;
            }
        }

        return false;
    };

    /**
     * confirm Count Delete
     * @returns {*}
     */
    this.confirmCountDelete = function() {
        var message = 'Are you sure you want to delete the Count field? If you do, your report will no longer be aggregated.';

        return confirm(message);
    };

    /**
     * removeItem
     */
    this.removeItem = function(item) {
        if (item.isCountField() && !self.confirmCountDelete()) {
            self.getInterface().updateCheckboxes();
            return;
        }

        item.remove();

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
        self.getInterface().updateCheckboxes();
    };

    /**
     * removeAllItems
     */
    this.removeAllItems = function() {
        if (self.hasCountItem() && !self.confirmCountDelete()) {
            self.getInterface().updateCheckboxes();
            return;
        }

        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            items[i].remove();
        }

        self.resetControls();
        self.resetEmptyMessage();
        self.getInterface().updateCheckboxes();
    };

    /**
     * removeItemByPath
     * @param path
     * @param func
     */
    this.removeItemByPath = function(path, func) {
        var item = self.getItem(path, func);

        if (item) {
            self.removeItem(item);
        }
    };

    /**
     * resetSortOrder
     */
    this.resetSortOrder = function() {
        var items = self.getItems();

        for (var i = 0, length = items.length; i < length; i++) {
            items[i].setSortOrder(i + 1);
        }
    };

    /**
     * resetEmptyMessage
     */
    this.resetEmptyMessage = function() {
        var items        = self.getItems();
        var emptyMessage = self.getEmptyMessage();

        if (items.length) {
            emptyMessage.hide();
        } else {
            emptyMessage.show();
        }
    };

    /**
     * resetControls
     */
    this.resetControls = function() {
        var items = self.getItems();

        if (items.length) {
            self.controls.show();
        } else {
            self.controls.hide();
        }
    };

    /**
     * createItemIdx
     *
     * ensures each item has a unique index
     *
     * @returns {number|*}
     */
    this.createItemIdx = function() {
        var childrenLength = targetElement.children('li').length;
        if (childrenLength < 1) {
            childrenLength = 1;
        }

        if (self.itemIdx === null) {
            self.itemIdx = childrenLength - 1;
        } else {
            self.itemIdx++;
        }
        
        return self.itemIdx;
    };

    /**
     * getEmptyMessage
     *
     * @type {*}
     */
    this.getEmptyMessage = brown298.memoize(function() {
        var emptyMessage = jQuery('<p></p>');
        emptyMessage.addClass('empty');
        emptyMessage.hide();

        var text = 'No fields are chosen';
        emptyMessage.html(text);

        targetElement.parent().before(emptyMessage);

        return emptyMessage;
    });

    /**
     * getControls
     * @type {*}
     */
    this.getControls = brown298.memoize(function() {
        var controls = jQuery('<p></p>');
        controls.addClass('list-controls');
        controls.hide();

        targetElement.parent().prev().before(controls);

        self.controls = controls;
        return self.controls;
    });

    /**
     * addControl
     *
     * @param text
     * @param callback
     * @param title
     * @returns {*}
     */
    this.addControl = function(text, callback, title) {
        var link = brown298.getLink(text, callback);
        link.addClass('control');
        if (title) {
            link.attr('title', title);
        }

        var controls = self.getControls();
        controls.append(' ').append(link);

        return link;
    };


    /**
     * initializeReset
     */
    this.initializeReset = function() {
        var control = self.addControl('Reset', self.removeAllItems, 'Delete all');

        control.hover(function() {
            targetElement.addClass('reset-hovering');
        }, function() {
            targetElement.removeClass('reset-hovering');
        });
    };

    /**
     * initializeSortable
     */
    this.initializeSortable = function() {
        targetElement.sortable({
            axis: 'y',
            handle: '.move',
            update: function(event, ui) {
                self.resetSortOrder();
            }
        });
    };

    /**
     * initialize
     */
    this.initialize = function() {

        self.initializeSortable();
        self.initializeReset();

        self.resetSortOrder();
        self.resetControls();
        self.resetEmptyMessage();
    };

    this.initialize();
};