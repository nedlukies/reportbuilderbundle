<?php
namespace Brown298\ReportBuilderBundle\Form\Step;

use Brown298\ReportBuilderBundle\Form\FilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FilterStepType
 * @package Brown298\ReportBuilderBundle\Form\Step
 */
class FilterStepType extends AbstractStepType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('filters', 'collection', array(
            'type'         => new FilterType(),
            'allow_add'    => true,
            'allow_delete' => true,
            'by_reference' => false,
            'required'     => false,
        ));

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('validation_groups' => array('filter_step')));
    }

}
