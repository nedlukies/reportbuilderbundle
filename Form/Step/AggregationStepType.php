<?php
namespace Brown298\ReportBuilderBundle\Form\Step;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AggregationStepType
 * @package Brown298\ReportBuilderBundle\Form\Step
 */
class AggregationStepType extends AbstractStepType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('aggregated', 'choice', array(
            'choices'  => array(0 => 'Detail', 1 => 'Aggregate'),
            'label'    => 'Report Type',
            'expanded' => true,
            'required' => true,
        ));

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('validation_groups' => array('aggregation_step')));
    }

}