<?php
namespace Brown298\ReportBuilderBundle\Form\Step;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PublishStepType
 * @package Brown298\ReportBuilderBundle\Form\Step
 */
class PublishStepType extends AbstractStepType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label'      => 'Report Name',
                'max_length' => 150,
                'required'   => true,
                'attr'       => array('maxlength' => 150),
            ))
            ->add('shared', 'choice', array(
                'choices' => array(
                    0 => 'Available only to me',
                    1 => 'Available to all users'
                ),
                'required' => true,
                'label'    => 'Sharing',
            ));

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('validation_groups' => array('publish_step')));
    }

}