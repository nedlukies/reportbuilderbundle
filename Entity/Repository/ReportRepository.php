<?php
namespace Brown298\ReportBuilderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ReportRepository
 * @package Brown298\ReportBuilderBundle\Entity\Repository
 */
class ReportRepository extends EntityRepository
{
    /**
     * getOwnedForIndexQb
     *
     * @param bool $includeDeleted
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getOwnedForIndexQb($includeDeleted)
    {
        $qb = $this->_em->createQueryBuilder()
            ->select('r')
            ->from('Brown298\ReportBuilderBundle\Entity\BuiltReport', 'r')
            ->andWhere('r.system = false');

        if (!$includeDeleted) {
            $qb->andWhere('r.deleted = false');
        }

        return $qb;
    }

    /**
     * getSharedForIndexQb
     *
     * @param bool $includeDeleted
     * @param bool $canEditOthers
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getSharedForIndexQb($includeDeleted, $canEditOthers = false)
    {
        $qb = $this->_em->createQueryBuilder()
            ->select('r')
            ->from('Brown298\ReportBuilderBundle\Entity\BuiltReport', 'r')
            ->andWhere('r.system = false')
            ->andWhere('r.valid = true')
            ->andWhere('r.published = true');

        if (!$includeDeleted) {
            $qb->andWhere('r.deleted = false');
        }

        if (!$canEditOthers) {
            $qb->andWhere('r.shared = true')
                ->andWhere('r.finished = true');
        }

        return $qb;
    }

    /**
     * getSystemForIndexQb
     *
     * @param bool $includeDeleted
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getSystemForIndexQb($includeDeleted)
    {
        $qb = $this->_em->createQueryBuilder()
            ->select('r')
            ->from('Brown298\ReportBuilderBundle\Entity\Report', 'r')
            ->andWhere('r.system = true');

        if (!$includeDeleted) {
            $qb->andWhere('r.deleted = false');
        }

        return $qb;
    }
}