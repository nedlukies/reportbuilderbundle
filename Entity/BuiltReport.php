<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Brown298\ReportBuilderBundle\Common\Inflector;
use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;
use Brown298\ReportBuilderBundle\Validator\Constraints as ReportAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class BuiltReport
 * @package Brown298\ReportBuilderBundle\Entity
 *
 * @ORM\Entity
 * @ReportAssert\OnlySelections(groups={"fields_step", "filter_step", "order_step"})
 * @Assert\Callback(methods={"isFieldsStepValid"}, groups={"fields_step"})
 * @Assert\Callback(methods={"isOrderStepValid"}, groups={"order_step"})
 * @ReportAnnotation\Ignored
 */
class BuiltReport extends Report
{
    /**
     * Count function
     */
    const FUNCTION_COUNT = 'count';
    const FUNCTION_SUM   = 'sum';
    const FUNCTION_DATE  = 'date';

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     * @Assert\NotBlank(groups={"publish_step"})
     */
    protected $name;

    /**
     * @var boolean $aggregated
     *
     * @ORM\Column(name="aggregated", type="boolean", nullable=true)
     * @Assert\NotBlank(groups={"aggregation_step"})
     */
    protected $aggregated;

    /**
     * @var string $entity
     *
     * @ORM\Column(name="entity", type="string", length=150, nullable=true)
     * @Assert\NotBlank(groups={"entity_step"})
     */
    protected $entity;

    /**
     * @var ArrayCollection $fields
     *
     * @ORM\OneToMany(targetEntity="Field", mappedBy="builtReport",
     *     cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder" = "ASC"})
     */
    protected $fields;

    /**
     * @var boolean $fieldsDecided
     *
     * @ORM\Column(name="fields_decided", type="boolean")
     */
    protected $fieldsDecided = false;

    /**
     * @var ArrayCollection $filters
     *
     * @ORM\OneToMany(targetEntity="Filter", mappedBy="builtReport",
     *     cascade={"persist"}, orphanRemoval=true)
     */
    protected $filters;

    /**
     * @var boolean $filterDecided
     *
     * @ORM\Column(name="filter_decided", type="boolean")
     */
    protected $filterDecided = false;

    /**
     * @var ArrayCollection $order
     *
     * @ORM\OneToMany(targetEntity="Order", mappedBy="builtReport",
     *     cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder" = "ASC"})
     */
    protected $orders;

    /**
     * @var boolean $published
     *
     * @ORM\Column(name="published", type="boolean")
     */
    protected $published = false;

    /**
     * @var boolean $shared
     *
     * @ORM\Column(name="shared", type="boolean")
     */
    protected $shared = false;

    /**
     * @var boolean $finished
     *
     * @ORM\Column(name="finished", type="boolean")
     */
    protected $finished = false;

    /**
     * @var boolean $valid
     *
     * @ORM\Column(name="valid", type="boolean")
     */
    protected $valid = true;

    /**
     * @var \Brown298\ReportBuilderBundle\Meta\PropertySelection[] $selections
     */
    protected $selections;

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        $this->fields = new ArrayCollection;
        $this->filters = new ArrayCollection;
        $this->orders = new ArrayCollection;

        parent::__construct();
    }

    /**
     * getCompleteness
     *
     * @return string
     */
    public function getCompleteness()
    {
        if ($this->isDeleted()) {
            return 'deleted';
        }

        if (!$this->isValid()) {
            return 'invalid';
        }

        if (!$this->isFinished()) {
            return 'unfinished';
        }

        if (!$this->isPublished()) {
            return 'unpublished';
        }

        return 'published';
    }

    /**
     * isFieldsStepValid
     *
     * @param ExecutionContextInterface $context
     * @return null
     */
    public function isFieldsStepValid(ExecutionContextInterface $context)
    {
        if (!$this->hasNonCountFields()) {
            $message = 'Please choose at least one field';
            if ($this->hasCountFields()) {
                $message .= ' that is not a count field';
            }
        } else if (!$this->isSelectingMainEntity()) {
            $message = 'Please choose at least one field from the base entity';
        }

        if (isset($message)) {
            $propertyPath = sprintf('%s.fields', $context->getPropertyPath());
            $context->addViolationAt($propertyPath, $message);
        }
    }

    /**
     * hasNonCountFields
     *
     * @return boolean
     */
    public function hasNonCountFields()
    {
        $fields = $this->getFields();

        foreach ($fields as $field) {
            if (!$field->isCountField()) {
                return true;
            }
        }

        return false;
    }

    /**
     * isSelectingMainEntity
     *
     * @return boolean
     */
    public function isSelectingMainEntity()
    {
        $fields = $this->getFields();

        foreach ($fields as $field) {
            if ($field->isDirectlyConnected()) {
                return true;
            }
        }

        return false;
    }

    /**
     * isOrderStepValid
     *
     * @param ExecutionContextInterface $context
     * @return null
     */
    public function isOrderStepValid(ExecutionContextInterface $context)
    {
        $count = $this->getOrders()->count();

        if ($count === 0) {
            $propertyPath = sprintf('%s.orders', $context->getPropertyPath());
            $message      = 'Please choose at least one';

            $context->addViolationAt($propertyPath, $message);
        }
    }

    /**
     * getCountFields
     *
     * @return null
     */
    public function getCountFields()
    {
        $fields = $this->getFields();

        $countFields = array();
        foreach ($fields as $field) {
            if ($field->isCountField()) {
                $countFields[] = $field;
            }
        }

        return $countFields;
    }

    /**
     * hasCountFields
     *
     * @return boolean
     */
    public function hasCountFields()
    {
        return count($this->getCountFields()) > 0;
    }

    /**
     * addCountField
     *
     * @return null
     */
    public function addCountField()
    {
        $field = new Field();
        $field->setFunction(self::FUNCTION_COUNT);

        $this->fields[] = $field;
        $field->setBuiltReport($this);
    }

    /**
     * removeCountFields
     *
     * @return null
     */
    public function removeCountFields()
    {
        $fields = $this->getCountFields();

        foreach ($fields as $field) {
            $this->removeField($field);
        }
    }

    /**
     * getCountFilters
     *
     * @return null
     */
    public function getCountFilters()
    {
        $filters = $this->getFilters();

        $countFilters = array();
        foreach ($filters as $filter) {
            if ($filter->isCountFilter()) {
                $countFilters[] = $filter;
            }
        }

        return $countFilters;
    }

    /**
     * hasCountFilters
     *
     * @return boolean
     */
    public function hasCountFilters()
    {
        return count($this->getCountFilters()) > 0;
    }

    /**
     * addCountFilter
     *
     * @return null
     */
    public function addCountFilter()
    {
        $filter = new Filter;
        $filter->setFunction(self::FUNCTION_COUNT);

        $this->filters[] = $filter;
        $filter->setBuiltReport($this);
    }

    /**
     * removeCountFilters
     *
     * @return null
     */
    public function removeCountFilters()
    {
        $filters = $this->getCountFilters();

        foreach ($filters as $filter) {
            $this->removeFilter($filter);
        }
    }

    /**
     * getCountOrders
     *
     * @return null
     */
    public function getCountOrders()
    {
        $orders = $this->getOrders();

        $countOrders = array();
        foreach ($orders as $order) {
            if ($order->isCountOrder()) {
                $countOrders[] = $order;
            }
        }

        return $countOrders;
    }

    /**
     * hasCountOrders
     *
     * @return boolean
     */
    public function hasCountOrders()
    {
        return count($this->getCountOrders()) > 0;
    }

    /**
     * addCountOrder
     *
     * @return null
     */
    public function addCountOrder()
    {
        $order = new Order;
        $order->setFunction(self::FUNCTION_COUNT);

        $this->orders[] = $order;
        $order->setBuiltReport($this);
    }

    /**
     * removeCountOrders
     *
     * @return null
     */
    public function removeCountOrders()
    {
        $orders = $this->getCountOrders();

        foreach ($orders as $order) {
            $this->removeOrder($order);
        }
    }


    /**
     * setAggregated
     *
     * @param boolean $aggregated
     */
    public function setAggregated($aggregated)
    {
        $this->aggregated = $aggregated;
    }

    /**
     * isAggregated
     *
     * @return boolean
     */
    public function isAggregated()
    {
        return $this->aggregated;
    }

    /**
     * setEntity
     *
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * getEntity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * setFields
     *
     * @param ArrayCollection $fields
     */
    public function setFields($fields)
    {
        foreach ($fields as $field) {
            $field->setBuiltReport($this);
        }

        $this->fields = $fields;
    }

    /**
     * addField
     *
     * @param Field $field
     */
    public function addField(Field $field)
    {
        $field->setBuiltReport($this);
        $this->fields->add($field);
    }

    /**
     * removeField
     *
     * @param ReportField $field
     * @return null
     */
    public function removeField($field)
    {
        $fields = $this->getFields();
        $fields->removeElement($field);
        $field->setBuiltReport(null);
    }

    /**
     * getFields
     *
     * @return ArrayCollection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * setFieldsDecided
     *
     * @param boolean $fieldsDecided
     */
    public function setFieldsDecided($fieldsDecided)
    {
        $this->fieldsDecided = $fieldsDecided;
    }

    /**
     * isFieldsDecided
     *
     * @return boolean
     */
    public function isFieldsDecided()
    {
        return $this->fieldsDecided;
    }

    /**
     * setFilters
     *
     * @param ArrayCollection $filters
     */
    public function setFilters($filters)
    {
        foreach ($filters as $filter) {
            $filter->setBuiltReport($this);
        }

        $this->filters = $filters;
    }

    /**
     * addFilter
     *
     * @param Filter $filter
     */
    public function addFilter(Filter $filter)
    {
        $filter->setBuiltReport($this);
        $this->filters->add($filter);
    }

    /**
     * removeFilter
     *
     * @param ReportFilter $filter
     * @return null
     */
    public function removeFilter($filter)
    {
        $filters = $this->getFilters();
        $filters->removeElement($filter);
        $filter->setBuiltReport(null);
    }

    /**
     * getFilters
     *
     * @return ArrayCollection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * setFilterDecided
     *
     * @param boolean $filterDecided
     */
    public function setFilterDecided($filterDecided)
    {
        $this->filterDecided = $filterDecided;
    }

    /**
     * isFilterDecided
     *
     * @return boolean
     */
    public function isFilterDecided()
    {
        return $this->filterDecided;
    }

    /**
     * setOrders
     *
     * @param $orders
     */
    public function setOrders($orders)
    {
        foreach ($orders as $order) {
            $order->setBuiltReport($this);
        }

        $this->orders = $orders;
    }

    /**
     * addOrder
     *
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $order->setBuiltReport($this);
        $this->orders->add($order);
    }

    /**
     * removeOrder
     *
     * @param Order $order
     * @return null
     */
    public function removeOrder($order)
    {
        $orders = $this->getOrders();
        $orders->removeElement($order);
        $order->setBuiltReport(null);
    }

    /**
     * getOrders
     *
     * @return ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * setPublished
     *
     * @param boolean $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * isPublished
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * setShared
     *
     * @param boolean $shared
     */
    public function setShared($shared)
    {
        $this->shared = $shared;
    }

    /**
     * isShared
     *
     * @return boolean
     */
    public function isShared()
    {
        return $this->shared;
    }

    /**
     * setFinished
     *
     * @param boolean $finished
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    }

    /**
     * isFinished
     *
     * @return boolean
     */
    public function isFinished()
    {
        return $this->finished;
    }

    /**
     * setValid
     *
     * @param boolean $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    /**
     * isValid
     *
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }
}
