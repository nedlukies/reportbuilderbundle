<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportAssetInterface;
use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="report_field")
 * @Assert\Callback(methods={"isValid"}, groups={"fields_step"})
 * @ReportAnnotation\Ignored
 */
class Field implements ReportAssetInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var BuiltReport $builtReport
     *
     * @ORM\ManyToOne(targetEntity="BuiltReport", inversedBy="fields")
     * @ORM\JoinColumn(name="built_report_id", referencedColumnName="id")
     */
    protected $builtReport;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=1000, nullable=true)
     */
    protected $path;

    /**
     * @var string $function
     *
     * @ORM\Column(name="func", type="string", length=150, nullable=true)
     */
    protected $function;

    /**
     * @var string $alias
     *
     * @ORM\Column(name="alias", type="string", length=150, nullable=true)
     */
    protected $alias;

    /**
     * @var integer $sortOrder
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    protected $sortOrder;

    /**
     * @var date $createdOn
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    protected $createdOn;

    /**
     * @var datetime $updatedOn
     *
     * @ORM\Column(name="updated_on", type="datetime")
     */
    protected $updatedOn;

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        $this->sortOrder = 0;

        $this->setCreatedOn(new \DateTime());
        $this->setUpdatedOn(new \DateTime());
    }

    /**
     * String representation
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->isCountField()) {
            return 'COUNT';
        }

        if (strlen($this->alias) > 0) {
            return $this->getAlias();
        }

        $path = $this->getPath();
        return $path;
    }

    /**
     * isValid
     *
     * @param ExecutionContext $context
     * @return null
     */
    public function isValid(ExecutionContext $context)
    {
        $path = $this->getPath();
        $countField = $this->isCountField();

        if (!$path && !$countField) {
            $propertyPath = sprintf('%s.path', $context->getPropertyPath());
            $message = 'Please fill in the path field';
            $context->addViolationAt($propertyPath, $message);
        }
    }

    /**
     * isCountField
     *
     * @return boolean
     */
    public function isCountField()
    {
        return $this->function === BuiltReport::FUNCTION_COUNT;
    }

    /**
     * isDirectlyConnected
     *
     * @return boolean
     */
    public function isDirectlyConnected()
    {
        return !$this->isCountField() && strpos($this->path, '.') === false;
    }

    /**
     * getId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setBuiltReport
     *
     * @param BuiltReport $builtReport
     */
    public function setBuiltReport($builtReport)
    {
        $this->builtReport = $builtReport;
    }

    /**
     * getBuiltReport
     *
     * @return BuiltReport
     */
    public function getBuiltReport()
    {
        return $this->builtReport;
    }

    /**
     * setPath
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * getPath
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * setFunction
     *
     * @param string $function
     * @throws \InvalidArgumentException
     */
    public function setFunction($function)
    {
        $allowedFunctions = array(
            BuiltReport::FUNCTION_COUNT,
            BuiltReport::FUNCTION_SUM,
            BuiltReport::FUNCTION_DATE,
        );
        if (!is_null($function) && !in_array($function, $allowedFunctions) && strpos('%s', $function) === false) {
            throw new \InvalidArgumentException('Invalid function');
        }

        $this->function = $function;
    }

    /**
     * getFunction
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * setSortOrder
     *
     * @param integer $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * getSortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * setCreatedOn
     *
     * @param \DateTime $createdOn
     */
    public function setCreatedOn(\DateTime $createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * getCreatedOn
     *
     * @return datetime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * setUpdatedOn
     *
     * @param \DateTime $updatedOn
     */
    public function setUpdatedOn(\DateTime $updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * getUpdatedOn
     *
     * @return datetime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }


}
