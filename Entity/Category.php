<?php
namespace Brown298\ReportBuilderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Brown298\ReportBuilderBundle\Entity\Report;
use Brown298\ReportBuilderBundle\Mapping\Annotation as ReportAnnotation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Category
 * @package Brown298\ReportBuilderBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="report_category")
 * @ReportAnnotation\Ignored
 */
class Category
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection $reports
     *
     * @ORM\OneToMany(targetEntity="Report", mappedBy="reportCategory")
     */
    protected $reports;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=150)
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var date $createdOn
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    protected $createdOn;

    /**
     * @ORM\Column(name="created_by", type="string", length=255, nullable=true)
     */
    protected $createdBy;

    /**
     * @var \DateTime $updatedOn
     *
     * @ORM\Column(name="updated_on", type="datetime")
     */
    protected $updatedOn;

    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     */
    protected $updatedBy;

    /**
     * @var boolean $deleted
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    protected $deleted = false;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;

    /**
     * Constructor
     *
     * @return \Brown298\ReportBuilderBundle\Entity\Category
     */
    public function __construct()
    {
        $this->reports = new ArrayCollection;

        $this->setCreatedOn(new \DateTime());
        $this->setUpdatedOn(new \DateTime());
    }

    /**
     * String representation
     *
     * @return string
     */
    public function __toString()
    {
        $name = $this->getName();
        if (!empty($name)) {
            return $name;
        }

        $id = $this->getId();
        if (!empty($id)) {
            return sprintf('Category %s', $id);
        }

        $name = 'Category';
        return $name;
    }

    /**
     * getId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setReports
     *
     * @param ArrayCollection $reports
     */
    public function setReports($reports)
    {
        foreach ($reports as $report) {
            $report->setCategory($this);
        }

        $this->reports = $reports;
    }

    /**
     * addReport
     *
     * @param Report $report
     */
    public function addReport(Report $report)
    {
        $report->setCategory($this);
        $this->reports->add($report);
    }

    /**
     * removeReport
     *
     * @param Report $report
     * @return null
     */
    public function removeReport($report)
    {
        $reports = $this->getReports();
        $reports->removeElement($report);
        $report->setCategory(null);
    }

    /**
     * getReports
     *
     * @return ArrayCollection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * setName
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setCreatedOn
     *
     * @param \DateTime $createdOn
     */
    public function setCreatedOn(\DateTime $createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * getCreatedOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * setUpdatedOn
     *
     * @param \DateTime $updatedOn
     */
    public function setUpdatedOn(\DateTime $updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * getUpdatedOn
     *
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * setDeleted
     *
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * isDeleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * setActive
     *
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * isActive
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        $this->setCreatedOn(new \DateTime);
        $this->setUpdatedBy($createdBy);
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
        $this->setUpdatedOn(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
