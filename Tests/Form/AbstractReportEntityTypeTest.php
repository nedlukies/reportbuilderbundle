<?php
namespace Form;

use Doctrine\Common\Util\Inflector as DoctrineInflector;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class AbstractReportEntityTypeTest
 * @package Form
 */
abstract class AbstractReportEntityTypeTest extends AbstractTest
{
    /**
     * @var string
     */
    protected $formNameTemplate = 'Brown298\ReportBuilderBundle\Form\%sType';

    /**
     * @var string
     */
    protected $entityNameTemplate = 'Brown298\ReportBuilderBundle\Entity\%s';

    /**
     * @Mock
     * @var \Symfony\Component\Form\FormBuilderInterface
     */
    protected $formBuilder;

    /**
     * testBuildForm
     */
    public function testBuildForm()
    {
        Phake::when($this->formBuilder)->add(Phake::anyParameters())->thenReturn($this->formBuilder);

        $entity = $this->createForm();
        $entity->buildForm($this->formBuilder, array());

        foreach ($this->fields as $key => $value) {

            // Simple params
            if (is_numeric($key)) {
                $field = $value;
                $type = null;
                $options = array('attr' => array('class' => $field), 'required' => false);
            } else {
                // Advanced params
                $field = $key;
                $type = $value[0];
                $options = $value[1];
            }

            Phake::verify($this->formBuilder)->add($field, $type, $options);
        }

    }

    /**
     * testSetDefaultOptions
     */
    public function testSetDefaultOptions()
    {
        $resolver = Phake::mock('Symfony\Component\OptionsResolver\OptionsResolverInterface');
        $entity = $this->createForm();
        $entity->setDefaultOptions($resolver);

        Phake::verify($resolver)->setDefaults(Phake::capture($options));

        $entityName = $this->getEntityName();
        $class = sprintf($this->entityNameTemplate, $entityName);
        $expected = array('data_class' => $class);
        $this->assertEquals($expected, $options);
    }

    /**
     * testGetName
     */
    public function testGetName()
    {
        $entity     = $this->createForm();
        $name       = $entity->getName();
        $entityName = $this->getEntityName();
        $expected   = DoctrineInflector::tableize($entityName);

        $this->assertEquals($expected, $name);
    }

    /**
     * createForm
     *
     * @return object
     */
    protected function createForm()
    {
        $entityName = $this->getEntityName();
        $class = sprintf($this->formNameTemplate, $entityName);
        $entity = Phake::partialMock($class);
        return $entity;
    }

    /**
     * getEntityName
     *
     * @return string "ReportField"
     */
    protected function getEntityName()
    {
        $class = get_class($this);
        $class = substr($class, strrpos($class, '\\') + 1);
        $class = preg_replace('/TypeTest$/', '', $class);
        return $class;
    }
} 