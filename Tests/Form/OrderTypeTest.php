<?php
namespace Form;

/**
 * Class OrderTypeTest
 * @package Form
 */
class OrderTypeTest extends AbstractReportEntityTypeTest
{
    protected $entityName = 'Order';

    protected $fields = array(
        'path',
        'function',
        'ascending' => array(
            'choice',
            array(
                'attr'     => array('class' => 'ascending'),
                'choices'  => array(1 => 'Ascending', 0 => 'Descending'),
                'label'    => 'Direction',
                'required' => false,
            ),
        ),
    );
}