<?php
namespace Form;

/**
 * Class FilterTypeTest
 * @package Form
 */
class FilterTypeTest extends AbstractReportEntityTypeTest
{
    protected $entityName = 'Filter';

    protected $fields = array(
        'path', 'function', 'type', 'value', 'second_value'
    );
}