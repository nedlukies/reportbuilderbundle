<?php
namespace Filter;

use Doctrine\Common\Util\Inflector as DoctrineInflector;
use Brown298\ReportBuilderBundle\Filter\FilterTypeFactory;
use Phake;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class FilterTypeFactoryTest
 * @package Filter
 */
class FilterTypeFactoryTest extends AbstractTest
{
    /**
     * @var array
     */
    public $keys = array(
        'between_dates', 'between_datetimes', 'between_numbers', 'contains', 'empty', 'empty_number',
        'empty_text', 'ends_with', 'equal_to', 'equal_to_number', 'false', 'greater_than', 'greater_than_or_equal_to',
        'less_than', 'less_than_or_equal_to', 'not_between_dates', 'not_between_datetimes', 'not_between_numbers', 'not_empty',
        'not_empty_number', 'not_empty_text', 'not_equal_to', 'not_equal_to_number', 'on_date', 'on_datetime', 'on_or_after_date',
        'on_or_after_datetime', 'on_or_before_date', 'on_or_before_datetime', 'starts_with', 'true',
    );

    /**
     * testKeyList
     */
    public function testKeyList()
    {
        $this->assertEquals(
            $this->getNamesFromTestKeys(),
            $this->getNamesFromFileSystem()
        );
    }

    /**
     * testCreate
     */
    public function testCreate()
    {
        foreach ($this->keys as $key) {
            $filterType = FilterTypeFactory::create($key);
            $name = DoctrineInflector::classify($key);
            $class = sprintf(FilterTypeFactory::CLASS_NAME_PATTERN, $name);
            $this->assertEquals($class, get_class($filterType));
        }
    }

    /**
     * testCreateMultiple
     */
    public function testCreateMultiple()
    {
        $filterTypes = FilterTypeFactory::create($this->keys);

        $names = array();
        foreach ($filterTypes as $filterType) {
            $names[] = DoctrineInflector::classify($filterType->getKey());
        }
        sort($names);

        $this->assertEquals($this->getNamesFromTestKeys(), $names);
    }

    /**
     * testCreateBadKey
     *
     * @expectedException InvalidArgumentException
     */
    public function testCreateBadKey()
    {
        FilterTypeFactory::create('gobbledy-gook');
    }

    /**
     * testCreateEmptyArray
     */
    public function testCreateEmptyArray()
    {
        $filterTypes = FilterTypeFactory::create(array());
        $this->assertSame(array(), $filterTypes);
    }

    /**
     * testCreateBadArray
     *
     * @expectedException InvalidArgumentException
     */
    public function testCreateBadArray()
    {
        FilterTypeFactory::create(array('gobbledy-gook'));
    }

    /**
     * testCreateBadArray2
     *
     * @expectedException InvalidArgumentException
     */
    public function testCreateBadArray2()
    {
        FilterTypeFactory::create(array(1432));
    }

    /**
     * @return array
     */
    private function getNamesFromTestKeys()
    {
        $names = array();

        foreach ($this->keys as $key) {
            $names[] = DoctrineInflector::classify($key);
        }

        sort($names);
        return $names;
    }

    /**
     * getNamesFromFileSystem
     *
     * @return array
     */
    private function getNamesFromFileSystem()
    {
        $names = array();

        $typeDir = dirname(__FILE__).'/../../Filter/Type';
        $files = scandir($typeDir);
        foreach ($files as $file) {

            if (
                $file === 'AbstractFilterType.php'
                || !preg_match('/FilterType\.php$/', $file)
            ) {
                continue;
            }

            $name = $file;
            $name = preg_replace('/FilterType\.php$/', '', $name);

            $names[] = $name;
        }

        sort($names);
        return $names;
    }
}