<?php
namespace Filter\Type;

use Phake;

/**
 * Class NotEmptyFilterTypeTest
 *
 * @package Filter\Type
 */
class NotEmptyFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\NotEmptyFilterType';

    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        return array(
            array(true, 'testProperty IS NOT NULL'),
            array(false, 'testProperty IS NOT NULL'),
        );
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is not empty', $this->filterType->getOptionLabel());
    }
}