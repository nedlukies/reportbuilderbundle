<?php
namespace Filter\Type;

use Brown298\ReportBuilderBundle\Filter\Type\OnOrBeforeDateFilterType;
use Phake;

/**
 * Class OnOrBeforeDatetimeFilterTypeTest
 *
 * @package Filter\Type
 */
class OnOrBeforeDatetimeFilterTypeTest extends OnOrBeforeDateFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\OnOrBeforeDatetimeFilterType';

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is on or before', $this->filterType->getOptionLabel());
    }
}