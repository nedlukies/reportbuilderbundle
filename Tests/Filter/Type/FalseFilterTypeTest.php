<?php
namespace Filter\Type;

use Phake;

/**
 * Class FalseFilterTypeTest
 *
 * @package Filter\Type
 */
class FalseFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\FalseFilterType';

    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        return array(
            array(true, 'testProperty = false'),
            array(false, 'testProperty = false'),
        );
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is false', $this->filterType->getOptionLabel());
    }
} 