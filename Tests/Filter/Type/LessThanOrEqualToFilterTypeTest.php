<?php
namespace Filter\Type;

use Phake;

/**
 * Class LessThanOrEqualToFilterTypeTest
 *
 * @package Filter\Type
 * @author John Brown <john.brown@partnerweekly.com>
 */
class LessThanOrEqualToFilterTypeTest extends LessThanFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\LessThanOrEqualToFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->lte(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is less than or equal to', $this->filterType->getOptionLabel());
    }
} 