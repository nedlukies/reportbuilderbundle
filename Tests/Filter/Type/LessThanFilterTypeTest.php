<?php
namespace Filter\Type;

use Phake;

/**
 * Class LessThanFilterTypeTest
 *
 * @package Filter\Type
 * @author John Brown <john.brown@partnerweekly.com>
 */
class LessThanFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\LessThanFilterType';

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->lt(Phake::anyParameters())->thenReturn('comparison');
    }

    /**
     * applyFilterProvider
     *
     * @return mixed
     */
    function applyFilterProvider()
    {
        return array(
            array(true,  'comparison'),
            array(false, 'comparison'),
        );
    }

    /**
     * isFilterObjectValidProvider
     *
     */
    public function isFilterObjectValidProvider()
    {
        return array(
            array('test', false),
            array('', false),
            array(null, false),
            array('01', true),
        );
    }

    /**
     * testIsFilterObjectValid
     *
     * @dataProvider isFilterObjectValidProvider
     */
    public function testIsFilterObjectValid($value, $exptectedResult)
    {
        Phake::when($this->filter)->getValue()->thenReturn($value);
        Phake::when($this->filter)->getSecondValue()->thenReturn($value);
        $result = $this->filterType->isFilterObjectValid($this->filter);

        if ($exptectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is less than', $this->filterType->getOptionLabel());
    }
} 