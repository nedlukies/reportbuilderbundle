<?php
namespace Filter\Type;

use Brown298\ReportBuilderBundle\Filter\Type\OnDateFilterType;
use Phake;

/**
 * Class OnOrAfterDatetimeFilterTypeTest
 *
 * @package Filter\Type
 */
class OnDatetimeFilterTypeTest extends OnDateFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\OnDatetimeFilterType';

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is on', $this->filterType->getOptionLabel());
    }

}