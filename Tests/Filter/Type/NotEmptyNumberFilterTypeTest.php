<?php
namespace Filter\Type;

use Phake;

/**
 * Class NotEmptyNumberFilterTypeTest
 *
 * @package Filter\Type
 */
class NotEmptyNumberFilterTypeTest extends AbstractFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\NotEmptyNumberFilterType';


    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->expr)->not(Phake::anyParameters())->thenReturn('testProperty IS NOT NULL');
    }

    /**
     * applyFilterProvider
     *
     * @return array|mixed
     */
    public function applyFilterProvider()
    {
        return array(
            array(true, 'testProperty IS NOT NULL'),
            array(false, 'testProperty IS NOT NULL'),
        );
    }

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is not empty', $this->filterType->getOptionLabel());
    }
} 