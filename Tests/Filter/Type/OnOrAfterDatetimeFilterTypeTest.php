<?php
namespace Filter\Type;

use Phake;

/**
 * Class OnOrAfterDatetimeFilterTypeTest
 *
 * @package Filter\Type
 */
class OnOrAfterDatetimeFilterTypeTest extends OnOrAfterDateFilterTypeTest
{
    /**
     * @var string
     */
    protected $filterTypeName = 'Brown298\ReportBuilderBundle\Filter\Type\OnOrAfterDatetimeFilterType';

    /**
     * testGetOptionLabel
     *
     */
    public function testGetOptionLabel()
    {
        $this->assertEquals('is on or after', $this->filterType->getOptionLabel());
    }

}