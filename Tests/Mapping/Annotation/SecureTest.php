<?php
namespace Mapping\Annotation;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Secure;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class SecureTest
 * @package Mapping\Annotation
 */
class SecureTest extends AbstractTest
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetSetEmpty()
    {
        $secure = new Secure(array());
    }

    /**
     * @return array
     */
    public function getSetProvider()
    {
        return array(
            array('ROLE_USER', array('ROLE_USER')),
            array('ROLE_USER,ROLE_SUPER_ADMIN', array('ROLE_USER','ROLE_SUPER_ADMIN'))
        );
    }

    /**
     * @dataProvider getSetProvider
     *
     * @param $roles
     * @param $expectedResults
     */
    public function testGetSet($roles, $expectedResults)
    {
        $args = array('roles' => $roles);

        $secure = new Secure($args);

        $this->assertEquals($expectedResults, $secure->getRoles());
    }
} 