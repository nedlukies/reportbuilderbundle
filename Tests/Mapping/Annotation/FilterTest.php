<?php
namespace Mapping\Annotation;

use Brown298\ReportBuilderBundle\Mapping\Annotation\Filter;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class FilterTest
 * @package Mapping\Annotation
 */
class FilterTest extends AbstractTest
{

    /**
     * testGetSet
     *
     * ensures values are available
     */
    public function testGetSet()
    {
       $types = array(
           'equal_to',
           'between',
           'test',
           array('a test'),
           9876,
       );
       $columnType = 'test';

       $args = array(
           'types'      => $types,
           'columnType' => $columnType,
       );

       $filter = new Filter($args);
       $this->assertEquals($columnType, $filter->getColumnType());
       $this->assertEquals($types, $filter->getTypes());
       $this->assertTrue($filter->hasTypes());

    }

    /**
     * testGetSetEmpty
     *
     * ensures if no values are passed in, results are null
     */
    public function testGetSetEmpty()
    {
        $args = array();
        $filter = new Filter($args);

        $this->assertNull($filter->getColumnType());
        $this->assertNull($filter->getTypes());
        $this->assertFalse($filter->hasTypes());

    }

}
