<?php
namespace Mapping\MetaData;

use Phake;
use Brown298\ReportBuilderBundle\Mapping\MetaData\PropertySelection;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class PropertySelectionTest
 * @package Mapping\MetaData
 */
class PropertySelectionTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\PropertySelection
     */
    protected $child1;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\PropertySelection
     */
    protected $child2;

    /**
     * @var array
     */
    protected $children;

    /**
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\PropertySelection
     */
    protected $selection;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();

        $this->children = array($this->child1, $this->child2);

        $this->selection = Phake::partialMock('\Brown298\ReportBuilderBundle\Mapping\MetaData\PropertySelection');
    }

    /**
     * testGetSet
     */
    public function testGetSet()
    {
        $expected = 'test path';
        $this->assertNull($this->selection->getPath());
        $this->selection->setPath($expected);
        $this->assertSame($expected, $this->selection->getPath());

        $expected = 'test func';
        $this->assertNull($this->selection->getFunc());
        $this->selection->setFunc($expected);
        $this->assertSame($expected, $this->selection->getFunc());

        $expected = 'test label';
        $this->assertNull($this->selection->getLabel());
        $this->selection->setLabel($expected);
        $this->assertSame($expected, $this->selection->getLabel());

        $expected = 'test label path prefix';
        $this->assertNull($this->selection->getLabelPathPrefix());
        $this->selection->setLabelPathPrefix($expected);
        $this->assertSame($expected, $this->selection->getLabelPathPrefix());

        $this->assertSame(array(), $this->selection->getChildren());
        $this->selection->setChildren($this->children);
        $this->assertSame($this->children, $this->selection->getChildren());
    }

    /**
     * testIsBase
     *
     */
    public function testIsBase()
    {
        $this->selection->setPath('./');
        $this->assertFalse($this->selection->isBase());

        $this->selection->setPath('test');
        $this->assertTrue($this->selection->isBase());
    }

    /**
     * testHasChildrent
     *
     */
    public function testHasChildrent()
    {
        $this->assertFalse($this->selection->hasChildren());

        $this->selection->setChildren(array('test'));
        $this->assertTrue($this->selection->hasChildren());
    }

    /**
     * getOneDescendant
     *
     */
    public function testGetOneDescendant()
    {
        $this->assertNull($this->selection->getOneDescendant());

        $this->selection->setChildren(array($this->selection));
        Phake::When($this->selection)->hasChildren()->thenReturn(false);
        $this->assertEquals($this->selection, $this->selection->getOneDescendant());


        $this->selection->setChildren(array($this->selection));
        Phake::When($this->selection)->hasChildren()->thenReturn(true)->thenReturn(false);
        $this->assertEquals($this->selection, $this->selection->getOneDescendant());
    }
} 