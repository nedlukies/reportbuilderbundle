<?php
namespace Service;

use Phake;
use Brown298\TestExtension\Test\AbstractTest;

/**
 * Class RoleSecuritySerivceTest
 * @package Service
 */
class RoleSecurityServiceTest extends AbstractTest
{

    /**
     * @Mock
     * @var \Symfony\Component\Security\Core\SecurityContext
     */
    protected $securityContext;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface
     */
    protected $reportContainer;

    /**
     * setup
     */
    public function setUp()
    {
        parent::setUp();
        $this->service = Phake::partialMock('Brown298\ReportBuilderBundle\Service\Security\RoleSecurityService', $this->securityContext, $this->treeMetadata);
    }


    /**
     * testCreate
     *
     * ensures we don't get an exception in the constructor
     */
    public function testCreate()
    {
        $this->assertInstanceOf('Brown298\ReportBuilderBundle\Service\Security\RoleSecurityService', $this->service);
    }

} 