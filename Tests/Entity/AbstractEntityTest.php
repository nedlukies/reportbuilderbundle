<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;

use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class AbstractReportEntityTest
 * @package Brown298\ReportBuilderBundle\Tests\Entity
 */
abstract class AbstractReportEntityTest extends AbstractTest
{
    /**
     * @var entities
     */
    protected $entity;

    /**
     * @var \Brown298\ReportBuilderBundle\Entity\BuiltReport
     */
    protected $report;

    /**
     * @var \Brown298\ReportBuilderBundle\Entity\BuiltReport
     */
    protected $additionalReport;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var string
     */
    protected $class;

    /**
     * @Mock
     * @var \Symfony\Component\Validator\ExecutionContext
     */
    protected $context;

    /**
     * @var string
     */
    protected $classNameTemplate = 'Brown298\ReportBuilderBundle\Entity\%s';

    /**
     * setUp
     */
    public function setup()
    {
        parent::setUp();
        $this->entity = Phake::partialMock(sprintf($this->classNameTemplate, $this->class));
        $this->entity->setPath('first_name');

        $this->report = new BuiltReport;

        $this->additionalReport = new BuiltReport;
        $this->count = BuiltReport::FUNCTION_COUNT;
    }

    /**
     * testConstructor
     */
    public function testConstructor()
    {
        $this->assertEquals(0, $this->entity->getSortOrder());
        $this->assertNotNull($this->entity->getCreatedOn());
        $this->assertNotNull($this->entity->getUpdatedOn());
    }

    /**
     * testGettersAndSetters
     */
    public function testGettersAndSetters()
    {
        $id = 123;
        $this->setProtectedValue($this->entity, 'id', $id);
        $this->assertEquals($id, $this->entity->getId());

        // Report
        $this->entity->setBuiltReport($this->report);
        $this->assertSame($this->report, $this->entity->getBuiltReport());

        // Path
        $path = 'first_name';
        $this->entity->setPath($path);
        $this->assertSame($path, $this->entity->getPath());

        // Function
        $function = $this->count;
        $badFunction = 'gobbledygook';
        $this->entity->setFunction($function);
        $this->assertSame($function, $this->entity->getFunction());
        $this->setExpectedException('\InvalidArgumentException');
        $this->entity->setFunction($badFunction);
        $this->assertSame($function, $this->entity->getFunction());

        // Sort order
        $sortOrder = 135;
        $this->entity->setSortOrder($sortOrder);
        $this->assertSame($sortOrder, $this->entity->getSortOrder());
    }

    /**
     * testToString
     */
    public function testToString()
    {
        $path       = 'first_name';
        $entityName = sprintf($this->classNameTemplate, $this->class);
        $entity     = new $entityName;

        $entity->setPath($path);
        $entity->setFunction(null);

        $this->assertSame((string) $entity, $path);
        $entity->setPath(null);
        $entity->setFunction($this->count);
        $this->assertSame((string) $entity, 'COUNT');
    }

    /**
     * testIsValid
     */
    public function testIsValid()
    {
        $context = Phake::mock('Symfony\Component\Validator\ExecutionContext');
        $this->entity->setPath(null);
        $this->entity->setFunction(null);

        $this->entity->isValid($context);

        Phake::verify($context)->getPropertyPath();
        Phake::verify($context)->addViolationAt($this->anything(), 'Please fill in the path field');
    }

    /**
     * testMoreGettersAndSetters
     */
    public function testMoreGettersAndSetters()
    {
        // Sort order
        $sortOrder = 135;
        $this->entity->setSortOrder($sortOrder);
        $this->assertSame($sortOrder, $this->entity->getSortOrder());
    }
}