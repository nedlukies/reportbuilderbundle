<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;

use Brown298\ReportBuilderBundle\Entity\StaticReport;

/**
 * Class StaticReportTest
 * @package Brown298\ReportBuilderBundle\Tests\Entity
 */
class StaticReportTest extends ReportTest
{
    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->entity = new StaticReport();
    }

    /**
     * testService
     */
    public function testService()
    {
        $testServiceName = 'brown298.some_test_service';
        $this->entity->setService($testServiceName);
        $response = $this->entity->getService();
        $this->assertEquals($testServiceName, $response);
    }

    /**
     * testIsStatic
     */
    public function testIsStatic() {
        $this->assertTrue($this->entity->isStatic());
    }
}