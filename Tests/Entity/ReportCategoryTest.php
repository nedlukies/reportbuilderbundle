<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;


use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;
use Doctrine\Common\Collections\ArrayCollection;
use Brown298\ReportBuilderBundle\Entity\Category;
use Brown298\ReportBuilderBundle\Entity\StaticReport;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;

class ReportCategoryTest extends AbstractTest
{

    protected $entity;

    protected $reports;

    protected $additionalReport;

    /**
     * setUp
     */
    public function setUp()
    {
        $this->entity = new Category();

        $report1 = new StaticReport;
        $report2 = new BuiltReport;
        $this->reports = array($report1, $report2);

        $this->additionalReport = new BuiltReport;
    }

    /**
     * testConstructor
     */
    public function testConstructor()
    {
        $arrayCollectionClass = get_class(new ArrayCollection);
        $this->assertSame($arrayCollectionClass, get_class($this->entity->getReports()));
        $this->assertCount(0, $this->entity->getReports());
        $this->assertNotNull($this->entity->getCreatedOn());
        $this->assertNotNull($this->entity->getUpdatedOn());
    }

    /**
     * testGettersAndSetters
     */
    public function testGettersAndSetters()
    {
        // Id
        $this->assertEquals('NULL', gettype($this->entity->getId()));

        // Name
        $name = 'Report Category Name';
        $this->entity->setName($name);
        $this->assertSame($name, $this->entity->getName());

        // Created On
        $lastCreate = $this->entity->getCreatedOn();
        $this->entity->setCreatedOn(new \DateTime);
        $this->assertNotSame($lastCreate, $this->entity->getCreatedOn());

        // setReports
        $reports = new ArrayCollection($this->reports);
        $this->entity->setReports($reports);
        $this->assertSame($reports, $this->entity->getReports());

        // addReport
        $this->entity->addReport($this->additionalReport);
        $this->assertCount(3, $this->entity->getReports());

        // removeReport
        $this->entity->removeReport($reports->first());
        $this->assertCount(2, $this->entity->getReports());

        // Deleted
        $this->entity->setDeleted(false);
        $this->assertFalse($this->entity->isDeleted());

        // Active
        $this->entity->setActive(false);
        $this->assertFalse($this->entity->isActive());
    }

    /**
     * testToString
     */
    public function testToString()
    {
        $this->entity->setName(NULL);
        $this->assertSame((string) $this->entity, 'Category');

        $name = 'Nameasdfasdfiu';
        $this->entity->setName($name);
        $this->assertSame((string) $this->entity, $name);
    }

    /**
     * testToStringNameId
     */
    public function testToStringNameId()
    {
        $id = 123;
        $this->setProtectedValue($this->entity, 'id', $id);
        $this->assertEquals('Category '.$id, $this->entity->__toString());
    }
}