<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;


use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;
use Brown298\ReportBuilderBundle\Entity\Report;

class ReportTest extends AbstractTest
{
    /**
     * @var \Brown298\ReportBuilderBundle\Entity\Report
     */
    protected $entity;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Entity\Category
     */
    protected $mockCat;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->entity = new Report();
    }

    /**
     * testConstructor
     */
    public function testConstructor()
    {
        $this->assertFalse($this->entity->isDeleted());
        $this->assertFalse($this->entity->isSystem());
    }

    /**
     * testIsBuilt
     */
    public function testIsBuilt()
    {
        // this is the base class so this should be false
        $this->assertFalse($this->entity->isBuilt());
    }

    /**
     * testIsStatic
     */
    public function testIsStatic()
    {
        // this is the base class so this should be false
        $this->assertFalse($this->entity->isStatic());
    }

    /**
     * testSettersAndGetters
     */
    public function testSettersAndGetters()
    {

        $creationTime = $this->entity->getCreatedOn();
        $this->assertNotNull($creationTime);

        $this->assertEquals('Report', (string)$this->entity);

        $reportName = 'SomeReportName';
        $this->entity->setName($reportName);
        $response = $this->entity->getName();
        $this->assertEquals($reportName, $response);

        $this->assertEquals('SomeReportName', (string)$this->entity);

        $before = $this->entity->getCategory();
        $this->assertNull($before);
        $this->entity->setCategory($this->mockCat);
        $after = $this->entity->getCategory();
        $this->assertSame($this->mockCat, $after);

        $this->entity->setDeleted(true);
        $this->assertTrue($this->entity->isDeleted());

        $this->entity->setSystem(true);
        $this->assertTrue($this->entity->isSystem());

        $constName = 'SOME_CONST';
        $this->assertNull($this->entity->getConst());
        $this->entity->setConst($constName);
        $response = $this->entity->getConst();
        $this->assertEquals($constName, $response);
    }

    /**
     * testToStringNameId
     */
    public function testToStringNameId()
    {
        $id = 123;
        $this->setProtectedValue($this->entity, 'id', $id);
        $this->assertEquals('Report '.$id, $this->entity->__toString());
    }
}