<?php
namespace Brown298\ReportBuilderBundle\Tests\Entity;

use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;
use Doctrine\Common\Collections\ArrayCollection;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Entity\Field;
use Brown298\ReportBuilderBundle\Entity\Filter;
use Brown298\ReportBuilderBundle\Entity\Order;

/**
 * Class BuiltReportTest
 * @package Brown298\ReportBuilderBundle\Tests\Entity
 */
class BuiltReportTest extends AbstractTest
{
    /**
     * @var \Brown298\ReportBuilderBundle\Entity\BuiltReport
     */
    protected $entity;

    /**
     * @var
     */
    protected $fields;

    /**
     * @var Field
     */
    protected $countField;

    /**
     * @var
     */
    protected $filters;

    /**
     * @var Filter
     */
    protected $countFilter;

    /**
     * @var
     */
    protected $orders;

    /**
     * @var Order
     */
    protected $countOrder;

    /**
     * setUp
     */
    public function setup()
    {
        $this->entity = new BuiltReport();

        $field1 = new Field;
        $field1->setPath('first_name');
        $field2 = new Field;
        $field2->setPath('last_name');
        $this->fields = array($field1, $field2);

        $this->countField = new Field;
        $this->countField->setFunction('count');

        $this->filters = array(new Filter, new Filter);

        $this->countFilter = new Filter;
        $this->countFilter->setFunction('count');

        $this->orders = array(new Order, new Order);

        $this->countOrder = new Order;
        $this->countOrder->setFunction('count');
    }

    /**
     * testConstructor
     *
     */
    public function testConstructor()
    {
        $arrayCollectionClass = get_class(new ArrayCollection);
        $this->assertSame($arrayCollectionClass, get_class($this->entity->getFields()));
        $this->assertCount(0, $this->entity->getFields());
        $this->assertFalse($this->entity->isFieldsDecided());
        $this->assertSame($arrayCollectionClass, get_class($this->entity->getFilters()));
        $this->assertCount(0, $this->entity->getFilters());
        $this->assertFalse($this->entity->isFilterDecided());
        $this->assertSame($arrayCollectionClass, get_class($this->entity->getOrders()));
        $this->assertCount(0, $this->entity->getOrders());
        $this->assertFalse($this->entity->isPublished());
        $this->assertFalse($this->entity->isShared());
        $this->assertFalse($this->entity->isFinished());
        $this->assertTrue($this->entity->isValid());

        // Parent constructor
        $this->assertFalse($this->entity->isDeleted());
        $this->assertFalse($this->entity->isSystem());
    }

    /**
     * testIsBuilt
     *
     */
    public function testIsBuilt()
    {
        $this->assertTrue($this->entity->isBuilt());
    }

    /**
     * testIsStatic
     *
     */
    public function testIsStatic()
    {
        $this->assertFalse($this->entity->isStatic());
    }

    /**
     * testSettersAndGetters
     */
    public function testSettersAndGetters()
    {
        // Aggregated
        $this->entity->setAggregated(true);
        $this->assertTrue($this->entity->isAggregated());

        // setFields
        $fields = new ArrayCollection($this->fields);
        $this->entity->setFields($fields);
        $this->assertSame($fields, $this->entity->getFields());

        // addField
        $this->entity->addCountField();
        $this->assertCount(3, $this->entity->getFields());

        // removeField
        $this->entity->removeField($fields->first());
        $this->assertCount(2, $this->entity->getFields());

        // removeCountFields
        $this->entity->removeCountFields();
        $this->assertCount(1, $this->entity->getFields());
        $this->entity->addField($this->countField);

        // filtersDecided
        $this->entity->setFieldsDecided(true);
        $this->assertTrue($this->entity->isFieldsDecided());

        // setFilters
        $filters = new ArrayCollection($this->filters);
        $this->entity->setFilters($filters);
        $this->assertSame($filters, $this->entity->getFilters());

        // addFilter
        $this->entity->addCountFilter();
        $this->assertCount(3, $this->entity->getFilters());

        // removeFilter
        $this->entity->removeFilter($filters->first());
        $this->assertCount(2, $this->entity->getFilters());

        // removeCountFilters
        $this->entity->removeCountFilters();
        $this->assertCount(1, $this->entity->getFilters());
        $this->entity->addFilter($this->countFilter);

        // filterDecided
        $this->entity->setFilterDecided(true);
        $this->assertTrue($this->entity->isFilterDecided());

        // setOrders
        $orders = new ArrayCollection($this->orders);
        $this->entity->setOrders($orders);
        $this->assertSame($orders, $this->entity->getOrders());

        // addOrder
        $this->entity->addCountOrder();
        $this->assertCount(3, $this->entity->getOrders());

        // removeOrder
        $this->entity->removeOrder($orders->first());
        $this->assertCount(2, $this->entity->getOrders());

        // removeCountOrders
        $this->entity->removeCountOrders();
        $this->assertCount(1, $this->entity->getOrders());
        $this->entity->addOrder($this->countOrder);

        // Published
        $this->entity->setPublished(false);
        $this->assertFalse($this->entity->isPublished());

        // Shared
        $this->entity->setShared(false);
        $this->assertFalse($this->entity->isShared());

        // Finished
        $this->entity->setFinished(false);
        $this->assertFalse($this->entity->isFinished());

        // Valid
        $this->entity->setValid(false);
        $this->assertFalse($this->entity->isValid());
    }

    /**
     * testGetComplete
     */
    public function testGetComplete()
    {
        // Base values
        $this->entity->setDeleted(false);
        $this->entity->setValid(true);
        $this->entity->setFinished(true);
        $this->entity->setPublished(true);

        // Each
        $this->assertEquals('published', $this->entity->getCompleteness());
        $this->entity->setPublished(false);
        $this->assertEquals('unpublished', $this->entity->getCompleteness());
        $this->entity->setFinished(false);
        $this->assertEquals('unfinished', $this->entity->getCompleteness());
        $this->entity->setValid(false);
        $this->assertEquals('invalid', $this->entity->getCompleteness());
        $this->entity->setDeleted(true);
        $this->assertEquals('deleted', $this->entity->getCompleteness());
    }

    /**
     * testIsFieldsStepValidNoFields
     */
    public function testIsFieldsStepValidNoFields()
    {
        // With no fields
        $context = Phake::mock('Symfony\Component\Validator\ExecutionContext');

        $fields = new ArrayCollection;
        $this->entity->setFields($fields);
        $this->entity->isFieldsStepValid($context);

        Phake::verify($context)->getPropertyPath();
        Phake::verify($context)->addViolationAt($this->anything(), 'Please choose at least one field');
    }

    /**
     * testIsFieldStepValidWithFields
     */
    public function testIsFieldStepValidWithFields()
    {
        // With count field
        $context = Phake::mock('Symfony\Component\Validator\ExecutionContext');

        $this->entity->getFields()->add($this->countField);
        $this->entity->isFieldsStepValid($context);

        Phake::verify($context)->getPropertyPath();
        Phake::verify($context)->addViolationAt($this->anything(), 'Please choose at least one field that is not a count field');
    }

    /**
     * testIsOrderStepValid
     */
    public function testIsOrderStepValid()
    {
        // With no orders
        $context = Phake::mock('Symfony\Component\Validator\ExecutionContext');

        $orders = new ArrayCollection;
        $this->entity->setOrders($orders);
        $this->entity->isOrderStepValid($context);

        Phake::verify($context)->getPropertyPath();
        Phake::verify($context)->addViolationAt($this->anything(), 'Please choose at least one');
    }

    /**
     * testCountDetectionFields
     */
    public function testCountDetectionFields()
    {
        // hasCountFields and hasNonCountFields
        $fields = new ArrayCollection(array($this->countField));
        $this->entity->setFields($fields);
        $this->assertTrue($this->entity->hasCountFields());
        $this->assertFalse($this->entity->hasNonCountFields());

        $fields = new ArrayCollection($this->fields);
        $this->entity->setFields($fields);
        $this->assertFalse($this->entity->hasCountFields());
        $this->assertTrue($this->entity->hasNonCountFields());

        // hasCountFilters
        $filters = new ArrayCollection(array($this->countFilter));
        $this->entity->setFilters($filters);
        $this->assertTrue($this->entity->hasCountFilters());

        $filters = new ArrayCollection($this->filters);
        $this->entity->setFilters($filters);
        $this->assertFalse($this->entity->hasCountFilters());

        // hasCountOrders
        $orders = new ArrayCollection(array($this->countOrder));
        $this->entity->setOrders($orders);
        $this->assertTrue($this->entity->hasCountOrders());

        $orders = new ArrayCollection($this->orders);
        $this->entity->setOrders($orders);
        $this->assertFalse($this->entity->hasCountOrders());
    }
}