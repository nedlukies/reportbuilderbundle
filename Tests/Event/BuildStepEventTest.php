<?php
namespace Event;

use \Phake;
use \Brown298\TestExtension\Test\AbstractTest;
use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class BuildStepEventTest
 * @package Event
 */
class BuildStepEventTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Entity\BuiltReport
     */
    protected $report;

    /**
     * @Mock
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Service\Security\NullSecurityService
     */
    protected $security;

    /**
     * @var string
     */
    protected $class = 'test';

    /**
     * @Mock
     * @var \Brown298\ReportBuilderBundle\Steps\AggregationStep
     */
    protected $step;

    /**
     * testConstructor
     */
    public function testConstructor()
    {
        $entity = new BuildStepEvent($this->report, $this->step, $this->em, $this->security, $this->class);

        $this->assertSame($this->report, $entity->getReport());
        $this->assertSame($this->step, $entity->getStep());
        $this->assertSame($this->em, $entity->getEm());
        $this->assertSame($this->security, $entity->getSecurity());
        $this->assertSame($this->class, $entity->getClass());
    }

} 