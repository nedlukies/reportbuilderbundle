<?php
namespace Brown298\ReportBuilderBundle\Twig;

use Brown298\ReportBuilderBundle\Service\ReportService;
use Brown298\ReportBuilderBundle\Service\StepService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig_Extension;
use Twig_SimpleFunction;
use Twig_Environment;

/**
 * Class StepNavigationExtension
 * @package Brown298\ReportBuilderBundle\Twig
 */
class StepNavigationExtension extends Twig_Extension
{
    /*
     * @var Container
     */
    private $container;

    /**
     * @var \Brown298\ReportBuilderBundle\Service\StepService
     */
    protected $stepService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var \Brown298\ReportBuilderBundle\Service\ReportService
     */
    protected $reportService;

    /**
     * @var null|string
     */
    protected $navigationTemplate = 'Brown298ReportBuilderBundle::stepNavigation.html.twig';

    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * Constructor
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     * @param Router                                                    $router
     * @param null|string                                               $navigationTemplate
     *
     * @return \Brown298\ReportBuilderBundle\Twig\StepNavigationExtension
     */
    public function __construct(ContainerInterface $container, RouterInterface $router, $navigationTemplate = null)
    {
        $this->container     = $container;
        $this->router        = $router;
        if ($navigationTemplate) {
            $this->navigationTemplate = $navigationTemplate;
        }
    }

    /**
     * @param Twig_Environment $environment
     */
    public function initRuntime(Twig_Environment $environment)
    {
        $this->environment   = $environment;
    }

    /**
     * getFunctions
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('get_step_nav', array($this, 'getNavigationList'), array('is_safe' => array('html'))),
            new Twig_SimpleFunction('get_current_step_label', array($this, 'getCurrentStepLabel'), array('is_safe' => array('html'))),
        );
    }

    /**
     * getNavigationList
     *
     * @return string
     */
    public function getNavigationList()
    {
        $this->stepService   = $this->container->get('brown298.report_builder.build.step');
        $this->reportService = $this->container->get('brown298.report_builder.report');
        $currentStep = $this->stepService->getCurrentStep();
        $report      = $this->reportService->getReport();
        $steps       = $this->stepService->getSteps();
        $args        = array();

        /** @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\StepInterface $step */
        foreach($steps as $step) {
            $stepKey    = $step->getKey();
            $accessible = $this->stepService->isStepAccessible($stepKey);

            $args['steps'][] = array(
                'accessible' => $accessible,
                'stepKey'    => $stepKey,
                'report'     => $report,
                'step'       => $step,
                'current'    => ($step === $currentStep),
            );
        }

        return $this->environment->render($this->navigationTemplate, $args);
    }

    /**
     * getCurrentStep
     *
     * @return string
     */
    public function getCurrentStepLabel()
    {
        $this->stepService   = $this->container->get('brown298.report_builder.build.step');
        /** @var \Brown298\ReportBuilderBundle\Mapping\Interfaces\StepInterface $currentStep */
        $currentStep = $this->stepService->getCurrentStep();
        $label = $currentStep->getLabel();
        $label = htmlspecialchars($label, ENT_QUOTES, 'UTF-8');

        return $label;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'brown298.report_builder.step_navigation';
    }
}
