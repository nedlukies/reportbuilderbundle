<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * Class OnOrBeforeDatetimeFilterType
 * @package Brown298\ReportBuilderBundle\Filter\Type
 */
class OnOrBeforeDatetimeFilterType extends OnOrBeforeDateFilterType implements FilterTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false )
    {
        $paramKey = $this->createQbParameterKey($qb);
        $value = $filter->getValue();

        $expr = $qb->expr()->lte($property, '?'.$paramKey);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        if (strpos($value, ':') === 0) {
            $qb->setParameter($paramKey, $value);
        } else {
            $date = new \DateTime($value);
            $qb->setParameter($paramKey, $date->format('Y-m-d H:i:s'));
        }

        return $qb;
    }
}