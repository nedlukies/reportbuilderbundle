<?php
namespace Brown298\ReportBuilderBundle\Filter\Type;

use Doctrine\ORM\QueryBuilder;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\FilterTypeInterface;
use Brown298\ReportBuilderBundle\Entity\Filter;

/**
 * OnOrBeforeDateFilterType
 *
 */
class OnOrBeforeDateFilterType extends AbstractFilterType implements FilterTypeInterface
{
    /**
     * @var string
     */
    protected $optionLabel = 'is on or before';

    /**
     * @var string
     */
    protected $formTemplate = 'Brown298ReportBuilderBundle:Build:Filter/one-text-input.html.twig';

    /**
     * isFilterObjectValid
     *
     * @param Filter $filter
     * @return bool
     */
    public function isFilterObjectValid(Filter $filter)
    {
        $value = $filter->getValue();

        if (!is_string($value)) {
            $this->addValidationError('value', 'Invalid argument');
            return false;
        }

        if (!$this->isDateValid($value) && strpos($value, ':') !== 0) {
            $this->addValidationError('value', 'Please enter a valid date');
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function applyFilter(QueryBuilder $qb, Filter $filter, $property, $having = false) {
        $paramKey = $this->createQbParameterKey($qb);
        $value = $filter->getValue();

        $expr = $qb->expr()->lte($property, '?'.$paramKey);

        if ($having) {
            $qb->andHaving($expr);
        } else {
            $qb->andWhere($expr);
        }

        if (strpos($value, ':') === 0) {
            $qb->setParameter($paramKey, $value);
        } else {
            $date = new \DateTime($value);
            $qb->setParameter($paramKey, $date->format('Y-m-d H:i:s'));
        }

        return $qb;
    }
}