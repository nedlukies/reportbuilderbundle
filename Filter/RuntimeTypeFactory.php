<?php
namespace Brown298\ReportBuilderBundle\Filter;

use Doctrine\Common\Util\Inflector;
use InvalidArgumentException;

/**
 * Class RuntimeTypeFactory
 * @package Brown298\ReportBuilderBundle\Filter
 */
class RuntimeTypeFactory extends AbstractTypeFactory
{
    /**
     * Pattern to find names of filter type classes
     */
    public static $classNamePattern = 'Brown298\ReportBuilderBundle\Filter\Runtime\%sRuntimeType';
}