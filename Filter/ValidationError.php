<?php
namespace Brown298\ReportBuilderBundle\Filter;

/**
 * Class ValidationError
 * @package Brown298\ReportBuilderBundle\Filter
 */
class ValidationError
{
    /**
     * @var string
     */
    protected $field;

    /**
     * @var string
     */
    protected $message;

    /**
     * Constructor
     *
     * @param string $field
     * @param string $message
     */
    public function __construct($field, $message)
    {
        $this->field = $field;
        $this->message = $message;
    }

    /**
     * getField
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * getMessage
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}