<?php
namespace Brown298\ReportBuilderBundle\Filter\Runtime;

use Brown298\ReportBuilderBundle\Common\Util\Inflector;

/**
 * Class AbstractRuntimeType
 * @package Brown298\ReportBuilderBundle\Filter\Runtime
 */
abstract class AbstractRuntimeType
{

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    protected $optionLabel;

    /**
     * Constructor
     *
     * @param mixed $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * getKey
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }


    /**
     * getLabel
     *
     * @return string
     */
    public function getLabel()
    {
        $key = $this->getKey();
        $label = Inflector::labelize($key);
        return $label;
    }

    /**
     * getOptionLabel
     *
     * @return string
     */
    public function getOptionLabel()
    {
        if ($this->optionLabel) {
            return $this->optionLabel;
        }

        $label = $this->getLabel();
        $label = strtolower($label);
        return $label;
    }
}