<?php
namespace Brown298\ReportBuilderBundle\EventListener;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;

/**
 * Class BuildStepListener
 * @package Brown298\ReportBuilderBundle\EventListener
 */
class BuildStepListener
{
    /**
     * onBuildStepPreBind
     *
     * @param BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPreBind(BuildStepEvent $event)
    {
        $step = $event->getStep();
        $method = 'onBuildStepPreBind';

        if (is_callable(array($step, $method))) {
            $step->$method($event);
        }
    }

    /**
     * onBuildStepPersist
     *
     * @param BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPersist(BuildStepEvent $event)
    {
        $step = $event->getStep();
        $method = 'onBuildStepPersist';

        if (is_callable(array($step, $method))) {
            $step->$method($event);
        }
    }
}