<?php
namespace Brown298\ReportBuilderBundle\EventListener;

use Brown298\ReportBuilderBundle\Event\BuildStepEvent;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Service\PropertySelectionService;
use Brown298\ReportBuilderBundle\Service\StepService;

/**
 * Class SelectionEnforcer
 * @package Brown298\ReportBuilderBundle\EventListener
 */
class SelectionEnforcer
{
    /**
     * @var PropertySelectionService
     */
    protected $selectionService;

    /**
     * Constructor
     *
     * @param  PropertySelectionService $selectionService
     */
    public function __construct(PropertySelectionService $selectionService)
    {
        $this->selectionService = $selectionService;
    }

    /**
     * onBuildStepPersist
     *
     * @param  BuildStepEvent $event
     * @return null
     */
    public function onBuildStepPersist(BuildStepEvent $event)
    {
        $report = $event->getReport();

        $this->enforce($report);
    }

    /**
     * enforce
     *
     * @param  BuiltReport $report
     * @return null
     */
    public function enforce(BuiltReport $report)
    {
        $this->selectionService->setReport($report);

        if (!$this->selectionService->canReportBeSelected()) {
            return;
        }

        $invalid = $this->selectionService->getInvalidReportAssets(StepService::STEP_FIELDS);

        if ($invalid) {
            foreach ($invalid as $asset) {
                $report->removeField($asset);
            }

            $report->setFieldsDecided(false);
        }

        $invalid = $this->selectionService->getInvalidReportAssets(StepService::STEP_FILTER);

        if ($invalid) {
            foreach ($invalid as $asset) {
                $report->removeFilter($asset);
            }

            $report->setFilterDecided(false);
        }

        $invalid = $this->selectionService->getInvalidReportAssets(StepService::STEP_ORDER);

        foreach ($invalid as $asset) {
            $report->removeOrder($asset);
        }
    }
}