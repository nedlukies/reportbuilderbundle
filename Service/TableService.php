<?php
namespace Brown298\ReportBuilderBundle\Service;

use Brown298\ReportBuilderBundle\DataTables\ReportDataTable;
use Doctrine\ORM\Query\QueryException;
use Exception;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Used mainly as a factory for table objects
 *
 */
class TableService
{
    /**
     * @var ReportContainerInterface
     */
    protected $reportContainer;

    /**
     * targetEm
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * setReportContainer
     *
     * @param ReportContainerInterface $reportContainer
     * @return null
     */
    public function setReportContainer(ReportContainerInterface $reportContainer)
    {
        $this->reportContainer = $reportContainer;
    }

    /**
     * @return ReportDataTable
     */
    public function getDataTable()
    {
        $this->assertReportContainer();
        $dataTable = new ReportDataTable($this->reportContainer);
        $dataTable->setContainer($this->container);
        $dataTable->setEm($this->em);

        return $dataTable;
    }

    /**
     * handleRequest
     *
     * @param  Request    $request
     * @return array|bool          Array of parameters to merge into route;
     *                             true if no action needed; false on form error
     */
    public function handleRequest(Request $request)
    {
        $this->assertReportContainer();
        $reportKey = $this->reportContainer->getKey();

        // Get URL parameters for every report table
        $urlReports = $request->attributes->get('report');
        $urlReports = $urlReports ? unserialize(base64_decode($urlReports)) : array();

        // Transfer URL parameters to report object
        if (isset($urlReports[$reportKey])) {
            $this->reportContainer->setParams($urlReports[$reportKey]);
        }

        if ('POST' === $request->getMethod()) {

            $form = $this->getParameterForm();

            if ($form) {
                $form->bindRequest($request);

                if (!$form->isValid()) {
                    return false;
                }

                $data = $form->getData();

                // Merge POST into new URL array
                $dataParams = array($reportKey => $data);
                $urlReports = array_replace($urlReports, $dataParams);
                $urlReports = base64_encode(serialize($urlReports));

                return array('report' => $urlReports);
            }
        }

        return true;
    }

    /**
     * getParameterForm
     *
     * @return Symfony\Component\Form\Form
     */
    public function getParameterForm()
    {
        $this->assertReportContainer();

        $form = $this->reportContainer->getParameterForm();
        return $form;
    }

    /**
     * hasParameterForm
     *
     * @return bool
     */
    public function hasParameterForm()
    {
        return (bool) $this->getParameterForm();
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        $this->assertReportContainer();

        return $this->reportContainer->getName();
    }

    /**
     * getName
     *
     * @throws Exception
     * @return null
     */
    private function assertReportContainer()
    {
        if (!$this->reportContainer) {
            throw new Exception('Report container not set');
        }
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return \Brown298\ReportBuilderBundle\Service\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }


}