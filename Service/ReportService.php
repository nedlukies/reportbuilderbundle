<?php
namespace Brown298\ReportBuilderBundle\Service;

use Doctrine\ORM\EntityManager;
use Brown298\ReportBuilderBundle\Entity\Report;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * ReportService
 *
 * Service to deliver a report if a $reportId parameter was passed to the
 * controller
 */
class ReportService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Report
     */
    private $report;

    /**
     * Constructor
     *
     * @param Request $request
     * @param EntityManager $em
     */
    public function __construct(Request $request, EntityManager $em)
    {
        $this->request = $request;
        $this->em      = $em;
    }

    /**
     * getBuiltReport
     *
     * @return Report
     * @throws NotFoundHttpException
     */
    public function getReport()
    {
        if (!$this->report && $this->request->attributes->has('reportId')) {
            $reportId = $this->request->attributes->get('reportId');
            $repo     = $this->em->getRepository('Brown298\ReportBuilderBundle\Entity\Report');
            $report   = $repo->find($reportId);

            if (!$report) {
                throw new NotFoundHttpException('Report not found');
            }

            $this->report = $report;
        }

        return $this->report;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'brown298.report_builder.report';
    }
}
