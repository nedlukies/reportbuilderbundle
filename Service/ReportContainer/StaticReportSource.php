<?php
namespace Brown298\ReportBuilderBundle\Service\ReportContainer;

use Doctrine\Common\Util\Inflector as DoctrineInflector;
use Exception;
use Brown298\ReportBuilderBundle\Service\ReportSourceService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

/**
 * Common code for all report source classes
 *
 */
abstract class StaticReportSource
{
    /**
     * @var string[]
     */
    protected $headerLabels;

    /**
     * @var string[]
     */
    protected $includedClasses = array();

    /**
     * @var mixed[string]
     */
    protected $params;

    /**
     * getIncludedClasses
     *
     * @return string[]
     */
    public function getIncludedClasses()
    {
        return $this->includedClasses;
    }

    /**
     * getHeaderLabels
     *
     * @throws Exception
     * @return string[]
     */
    public function getHeaderLabels()
    {
        if (!is_array($this->headerLabels) || !$this->headerLabels) {
            throw new Exception('Header labels not set');
        }

        return $this->headerLabels;
    }

    /**
     * getDataRows
     *
     * @throws Exception
     * @return null
     */
    public function getDataRows()
    {
        throw new Exception('Override this method in child class');
    }

    /**
     * setFormFactory
     *
     * @param FormFactory $formFactory
     * @return null
     */
    public function setFormFactory($formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * getKey
     *
     * @return string
     */
    public function getKey()
    {
        $key = get_class($this);
        $key = DoctrineInflector::tableize($key);
        return $key;
    }

    /**
     * getDefaultParams
     *
     * @return mixed[string]
     */
    public function getDefaultParams()
    {
        return array();
    }

    /**
     * getParameterFormOptions
     *
     * @return mixed[string]
     */
    public function getParameterFormOptions()
    {
        return array();
    }

    /**
     * getParameterForm
     *
     * @param null $data
     *
     * @return \Symfony\Component\Form\Form
     */
    public function getParameterForm($data = null)
    {
        $data = $this->getParams();
        $options = $this->getParameterFormOptions();

        $builder = $this->formFactory->createBuilder('form', $data, $options);
        $built = $this->buildParameterForm($builder, $options);

        if ($built === false) {
            return null;
        }

        $form = $builder->getForm();

        return $form;
    }

    /**
     * buildParameterForm
     *
     * @param  FormBuilder   $builder
     * @param  mixed[string] $options
     * @return boolean
     */
    public function buildParameterForm(FormBuilder $builder, array $options)
    {
        return false;
    }

    /**
     * Set parameter data
     *
     * @param  mixed[string] $data
     * @return null
     */
    public function setParams($data)
    {
        $this->params = $data;
    }

    /**
     * Get parameter data
     *
     * @return mixed[string]
     */
    public function getParams()
    {
        if ($this->params) {
            return $this->params;
        }

        return $this->getDefaultParams();
    }

    /**
     * Get parameter data
     *
     * @param $key
     *
     * @return string
     */
    public function getParam($key)
    {
        $params = $this->getParams();

        if (isset($params[$key])) {
            return $params[$key];
        }
    }
}