<?php
namespace Brown298\ReportBuilderBundle\Service\ReportContainer;

use Brown298\ReportBuilderBundle\Entity\StaticReport;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class StaticReportContainer
 *
 * @package Brown298\ReportBuilderBundle\Service\ReportContainer
 * @author John Brown <john.brown@partnerweekly.com>
 */
class StaticReportContainer extends AbstractReportContainer
{
    /**
     * @var StaticReport
     */
    protected $report;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var StaticReportSource
     */
    private $source;

    /**
     * Constructor
     *
     * @param StaticReport $report
     * @param ContainerInterface $container
     */
    public function __construct(
        StaticReport $report, ContainerInterface $container
    ) {
        parent::__construct($report);

        $this->container = $container;
    }

    /**
     * getSource
     *
     * @return AbstractReportSource
     */
    public function getSource()
    {
        if (!$this->source) {
            $serviceKey = $this->report->getService();
            $source = $this->container->get($serviceKey);
            $this->source = $source;
        }

        return $this->source;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->report->getName();
    }

    /**
     * getHeaderLabels
     *
     * @return array
     */
    public function getHeaderLabels()
    {
        return $this->getSource()->getHeaderLabels();
    }

    /**
     * getDataRows
     *
     * @return array
     */
    public function getDataRows()
    {
        return $this->getSource()->getDataRows();
    }

    /**
     * getIncludedClasses
     *
     * @return array
     */
    public function getIncludedClasses()
    {
        return $this->getSource()->getIncludedClasses();
    }

    /**
     * Get parameter form
     *
     * @return Symfony\Component\Form\AbstractType
     */
    public function getParameterForm()
    {
        return $this->getSource()->getParameterForm();
    }

    /**
     * Set parameter data
     *
     * @param array $data
     * @return null
     */
    public function setParams($data)
    {
        $this->getSource()->setParams($data);
    }
} 