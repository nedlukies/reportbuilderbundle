<?php
namespace Brown298\ReportBuilderBundle\Service\ReportContainer;

use Brown298\ReportBuilderBundle\Form\ParameterForm;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportContainerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use ErrorException;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;
use Brown298\ReportBuilderBundle\Mapping\MetaData\ClassMetadata;
use Symfony\Component\Form\FormFactory;

/**
 * Class BuiltReportContainer
 *
 * Consumes a built report entity and provides a consistent interface
 *
 * @package Brown298\ReportBuilderBundle\Service\ReportContainer
 */
class BuiltReportContainer extends AbstractReportContainer implements ReportContainerInterface
{
    /**
     * @var BuiltReport
     */
    protected $report;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TreeMetadata
     */
    private $treeMetadata;

    /**
     * @var ClassMetadata Used for memory caching
     */
    private $classMetadata;

    /**
     * @var AbstractReportSource
     */
    private $source;

    /**
     * @var array
     */
    public $validityErrors = array();

    /**
     * @var \Symfony\Component\Form\FormFactory $formFactory
     */
    protected $formFactory;

    /**
     * Constructor
     *
     * @param BuiltReport $report
     * @param EntityManager $em
     * @param TreeMetadata $treeMeta
     */
    public function __construct(FormFactory $formFactory,  BuiltReport $report, EntityManager $em, TreeMetadata $treeMeta )
    {
        parent::__construct($report);

        $this->em = $em;
        $this->treeMetadata = $treeMeta;
        $this->formFactory  = $formFactory;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->report->getName();
        return $name;
    }

    /**
     * getColumns
     *
     * @return array
     */
    public function getColumns()
    {
        /** @var ReportQueryBuilder $qb  */
        $qb        = $this->createQueryBuilder();
        $classMeta = $this->getClassMetadata();
        $selects   = $qb->getSelectFields();

        $row = array();
        /** @var \Brown298\ReportBuilderBundle\Service\ReportContainer\SelectField $select */
        foreach ($selects as $select) {
            $pathArray  = explode('.', $select->reportField->getPath());
            $aliasArray = explode('_', $select->alias);
            $shortPath = array_pop($pathArray);
            $path = sprintf("%s.%s", array_shift($aliasArray), $shortPath);

            if ($select->reportField->isCountField()) {
                $label = 'Count';
            } elseif (strlen($select->reportField->getAlias()) > 0) {
                $label = $select->reportField->getAlias();
            } else {
                $propertyMeta = $classMeta->getPropertyMetadataByPath($select->reportField->getPath());

                $label     = $propertyMeta->getLabel();
                $labelPath = $propertyMeta->getLabelPathTruncated();

                if ($path) {
                    $label .= sprintf(' (%s)', $labelPath);
                }
            }
            $row[$path] = $label;
        }

        return $row;
    }

    /**
     * getHeaderLabels
     *
     * @return array
     */
    public function getHeaderLabels()
    {
        $fields    = $this->report->getFields();
        $classMeta = $this->getClassMetadata();

        $row = array();
        foreach ($fields as $field) {

            if ($field->isCountField()) {
                $label = 'Count';
            } else {
                $path = $field->getPath();
                $propertyMeta = $classMeta->getPropertyMetadataByPath($path);

                $label = $propertyMeta->getLabel();

                $path = $propertyMeta->getLabelPathTruncated();

                if ($path) {
                    $label .= sprintf(' (%s)', $path);
                }
            }

            $row[] = $label;
        }

        return $row;
    }

    /**
     * getDataRows
     *
     * @return array
     */
    public function getDataRows()
    {
        $qb = $this->createQueryBuilder();

        $rows = $qb->getData();

        return $rows;
    }

    /**
     * getIncludedClasses
     *
     * @return array
     */
    public function getIncludedClasses()
    {
        $classes = array();

        // Get class from entity
        $entity = $this->report->getEntity();
        if (!$entity) {
            return array();
        }
        $classes[] = $entity;

        // Get paths from fields and filters
        $paths = array();
        foreach ($this->report->getFields() as $field) {
            $path = $field->getPath();

            if ($path) {
                $paths[] = $path;
            }
        }
        foreach ($this->report->getFilters() as $filter) {
            $path = $filter->getPath();

            if ($path) {
                $paths[] = $path;
            }
        }

        // Find classes from collected paths
        $classMeta = $this->getClassMetadata();
        foreach ($paths as $path) {
            $more = $classMeta->getClassesInPath($path);
            $classes = array_merge($classes, $more);
        }

        $classes = array_unique($classes);
        $classes = array_values($classes);

        return $classes;
    }

    /**
     * createQueryBuilder
     *
     * @return BuiltReportQueryBuilder
     */
    public function createQueryBuilder()
    {
        $qb = new ReportQueryBuilder($this->em, $this->report, $this->getClassMetadata());

        return $qb;
    }

    /**
     * getClassMetadata
     *
     * @return ClassMetadata
     */
    private function getClassMetadata()
    {
        if (!$this->classMetadata) {
            $entity              = $this->report->getEntity();
            $this->classMetadata = $this->treeMetadata->getClassMetadata($entity);
        }

        return $this->classMetadata;
    }

    /**
     * analyzeValidity
     *
     * @return boolean
     */
    public function analyzeValidity()
    {
        $this->validityErrors = array();

        $entity = $this->report->getEntity();

        if (!$entity) {
            return true;
        }

        // Check entity
        if (!$this->treeMetadata->isClassValid($entity)) {
            $message = sprintf('Invalid entity: %s', $entity);
            $this->validityErrors[] = $message;
            return false;
        }

        // Check fields, filters, and orders
        $lists = array(
            'fields' => $this->report->getFields(),
            'filters' => $this->report->getFilters(),
            'orders' => $this->report->getOrders(),
        );

        $classMeta = $this->treeMetadata->getClassMeta($entity);
        foreach ($lists as $name => $list) {
            foreach ($list as $entity) {

                $path = $entity->getPath();

                if (!$path) {
                    continue;
                }

                if (!$classMeta->isValidPath($path)) {
                    $message = sprintf('Invalid path in %s: %s', $name, $path);
                    $this->validityErrors[] = $message;
                }
            }
        }

        return count($this->validityErrors) === 0;
    }



    /**
     * @return bool
     */
    public function hasParameters()
    {
        $hasParameter = false;
        $filters      = $this->report->getFilters();
        if (!$filters->isInitialized()) {
            $filters->initialize();
        }

        foreach ($filters as $filter) {
            if ($filter->isParameter()) {
                $hasParameter = true;
            }
        }

        return $hasParameter;
    }

    /**
     * @return mixed
     */
    public function getParameterForm()
    {
        $parameterFormBuilder = $this->getParameterFormBuilder();
        $parameterForm        = $this->formFactory->create($parameterFormBuilder, array());
        return $parameterForm;
    }

    /**
     * Get parameter form
     *
     * @return null
     */
    public function getParameterFormBuilder()
    {
        $parameters = array();
        $filters    = $this->report->getFilters();
        if (!$filters->isInitialized()) {
            $filters->initialize();
        }

        foreach ($filters as $filter) {
            if ($filter->isParameter()) {
                $parameters[] = $filter;
            }
        }

        $formBuilder = new ParameterForm($parameters);

        return $formBuilder;
    }
} 