<?php
namespace Brown298\ReportBuilderBundle\Service;


use InvalidArgumentException;
use ErrorException;
use Brown298\ReportBuilderBundle\Entity\BuiltReport;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\ReportAssetInterface;
use Brown298\ReportBuilderBundle\Mapping\MetaData\PropertySelection;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata;

/**
 * Class PropertySelectionService
 * @package Brown298\ReportBuilderBundle\Service
 */
class PropertySelectionService
{
    /**
     * @var TreeMetadata
     */
    protected $treeMetadata;

    /**
     * @var BuiltReport
     */
    protected $report;

    /**
     * @var string[][]
     */
    protected $stepInfo = array(
        StepService::STEP_FIELDS => array('assetGetter' => 'getFields'),
        StepService::STEP_FILTER => array('assetGetter' => 'getFilters'),
        StepService::STEP_ORDER  => array('assetGetter' => 'getOrders'),
    );

    /**
     * @var PropertySelection[string][]
     */
    protected $selectionsCache;

    /**
     * Constructor
     *
     * @param \Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetadata $treeMetadata
     */
    public function __construct(TreeMetadata $treeMetadata)
    {
        $this->treeMetadata = $treeMetadata;
    }

    /**
     * setReport
     *
     * @param  BuiltReport $report
     * @return null
     */
    public function setReport(BuiltReport $report = null)
    {
        $this->report = $report;
    }

    /**
     * getSelections
     *
     * @param $stepKey
     *
     * @return PropertySelection[]
     */
    public function getSelections($stepKey)
    {
        if (!isset($this->selectionsCache[$stepKey])) {
            $this->selectionsCache[$stepKey] = $this->getSelectionsInner($stepKey);
        }

        return $this->selectionsCache[$stepKey];
    }

    /**
     * canReportBeSelected
     *
     * @return boolean
     */
    public function canReportBeSelected()
    {
        $report = $this->getReport();

        return ($report->getEntity() !== null && $report->isAggregated() !== null);
    }

    /**
     * getInvalidReportAssets
     *
     * @param  string $stepKey
     * @return ReportAssetInterface[]
     */
    public function getInvalidReportAssets($stepKey)
    {
        $assets = $this->getReportAssets($stepKey);
        $return = array();

        foreach ($assets as $asset) {
            if (!$this->isValidReportAsset($asset, $stepKey)) {
                $return[] = $asset;
            }
        }

        return $return;
    }

    /**
     * isValidReportAsset
     *
     * @param  ReportAssetInterface $asset
     * @param  string               $stepKey
     * @return boolean
     */
    public function isValidReportAsset(ReportAssetInterface $asset, $stepKey)
    {
        $selections = $this->getSelections($stepKey);
        $path = $asset->getPath();
        $func = $asset->getFunction();

        foreach ($selections as $selection) {
            if ($selection != null && ($selection->find($path, $func) || $selection->find($path, null))) {
                return true;
            }
        }

        return false;
    }

    /**
     * createCountSelection
     *
     * @return PropertySelection
     */
    protected function createCountSelection()
    {
        $selection = new PropertySelection;
        $selection->setFunc(BuiltReport::FUNCTION_COUNT);
        $selection->setLabel('Count');

        return $selection;
    }

    /**
     * getReport
     *
     * @throws ErrorException
     * @return BuiltReport
     */
    protected function getReport()
    {
        if (!$this->report) {
            throw new ErrorException('Report not set in property selection service');
        }

        return $this->report;
    }

    /**
     * findSelections
     *
     * @param $method
     * @param null $context
     * @return PropertySelection[]
     */
    protected function findSelections($method, $context = null)
    {
        $return = array();
        $report = $this->getReport();
        $entity = $report->getEntity();

        if ($report->isAggregated()) {
            $return[] = $this->createCountSelection();
        }

        $classMetadata = $this->treeMetadata->getClassMetadata($entity);
        $selections    = $classMetadata->getSelections($method, $context);

        return array_merge($return, $selections);
    }

    /**
     * getStepInfo
     *
     * @param  string $stepKey
     * @param  string $key
     * @return mixed
     */
    protected function getStepInfo($stepKey = null, $key = null)
    {
        if ($stepKey !== null) {

            if ($key !== null) {
                return $this->stepInfo[$stepKey][$key];
            }

            return $this->stepInfo[$stepKey];
        }

        return $this->stepInfo;
    }

    /**
     * getSelectionsInner
     *
     *
     * @param $stepKey
     * @throws \InvalidArgumentException
     * @return PropertySelection[]
     */
    protected function getSelectionsInner($stepKey)
    {
        switch ($stepKey) {

            case StepService::STEP_FIELDS;
                return $this->findSelections('getPropertyMetadatasForTreeAggregatable');

            case StepService::STEP_FILTER;
                return $this->findSelections('getPropertyMetadatasForTree');

            case StepService::STEP_ORDER;

                $report = $this->getReport();

                return $this->findSelections('getPropertyMetadatasForTreeOnlyFields', array(
                    'report' => $report
                ));

            default:
                throw new InvalidArgumentException(sprintf('Invalid step key "%s"', $stepKey));
        }
    }

    /**
     * getReportAssets
     *
     * @param  string $stepKey
     * @return ReportAssetInterface[]
     */
    protected function getReportAssets($stepKey)
    {
        $report = $this->getReport();
        $assetGetter = $this->getStepInfo($stepKey, 'assetGetter');
        $assets = $report->$assetGetter();

        return $assets;
    }
}
