<?php
namespace Brown298\ReportBuilderBundle\Service;

use Brown298\ReportBuilderBundle\Entity\Report;
use Brown298\ReportBuilderBundle\Mapping\Interfaces\SecurityInterface;
use Doctrine\Common\Util\Inflector as DoctrineInflector;
use Exception;
use Brown298\ReportBuilderBundle\Mapping\MetaData\TreeMetaData;
use Symfony\Component\HttpFoundation\Request;

/**
 * StepService
 *
 */
class StepService
{
    /**
     * Step class
     */
    const STEP_FORM_CLASS = 'Brown298\ReportBuilderBundle\Form\Step\%sStepType';

    /**
     * @var string
     */
    protected $stepClassTemplate = 'Brown298\ReportBuilderBundle\Steps\%sStep';

    /**
     * Step keys
     */
    const STEP_AGGREGATION = 'aggregation';
    const STEP_ENTITY      = 'entity';
    const STEP_FIELDS      = 'fields';
    const STEP_FILTER      = 'filter';
    const STEP_ORDER       = 'order';
    const STEP_PUBLISH     = 'publish';

    /**
     * @var array
     */
    private $stepKeys;

    /**
     * @var ReportService
     */
    private $reportService;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var PropertySelectionService
     */
    protected $selectionService;

    /**
     * @var AbstractStep
     */
    private $currentStep;

    /**
     * @var AbstractStep[]
     */
    private $steps = array();

    /**
     * @var TreeMetaData
     */
    protected $treeMetadata;

    /**
     * Constructor
     *
     * @param  ReportService $reportService
     * @param  Request $request
     * @param PropertySelectionService $selectionService
     */
    public function __construct(
        ReportService            $reportService,
        Request                  $request,
        PropertySelectionService $selectionService,
        TreeMetadata             $treeMetaData
    ) {
        $this->reportService    = $reportService;
        $this->treeMetadata     = $treeMetaData;
        $this->request          = $request;
        $this->selectionService = $selectionService;
        $this->stepKeys         = array(self::STEP_AGGREGATION, self::STEP_ENTITY, self::STEP_FIELDS, self::STEP_FILTER,
                                        self::STEP_ORDER, self::STEP_PUBLISH );

        $report = $this->reportService->getReport();
        $this->loadReport($report);
    }

    /**
     * loadReport
     *
     * @param Report $report
     */
    public function loadReport(Report $report = null)
    {
        if ($report != null) {
            $this->selectionService->setReport($report);
            foreach ($this->stepKeys as $stepKey) {
                $this->addStep($stepKey);
            }
        }
    }

    /**
     * getCurrentStep
     *
     * @return AbstractStep
     * @throws Exception
     */
    public function getCurrentStep()
    {
        if (!$this->currentStep && $this->request->attributes->has('stepKey')) {

            $stepKey = $this->request->attributes->get('stepKey');

            foreach ($this->steps as $step) {
                if ($step->getKey() == $stepKey) {
                    $currentStep = $step;
                }
            }

            if (!isset($currentStep)) {
                $message = sprintf('Step %s not found', $stepKey);
                throw new Exception($message);
            }

            $this->currentStep = $currentStep;
        }

        return $this->currentStep;
    }

    /**
     * addStep
     *
     * @param $key
     * @throws \Exception
     * @internal param $step
     * @return self
     */
    public function addStep($key)
    {
        $class = DoctrineInflector::classify($key);
        $class = sprintf($this->stepClassTemplate, $class);
        if (!class_exists($class)) {
            $message = sprintf('Class %s does not exist', $class);
            throw new Exception($message);
        }

        $report = $this->reportService->getReport();
        $step = new $class($key, $report, $this->selectionService, $this->treeMetadata);

        $this->steps[] = $step;

        return $this;
    }

    /**
     * getFirstStep
     *
     * @return string
     */
    public function getFirstStep()
    {
        return reset($this->steps);
    }

    /**
     * getSteps
     *
     * @return array
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * getFirstUnfinishedStep
     *
     * @return string
     */
    public function getFirstUnfinishedStep()
    {
        foreach ($this->steps as $step) {
            if (!$step->isFinished()) {
                return $step;
            }
        }

        return null;
    }

    /**
     * isFinished
     *
     * @return bool
     */
    public function isFinished()
    {
        foreach ($this->steps as $step) {
            if (!$step->isFinished()) {
                return false;
            }
        }

        return true;
    }

    /**
     * isViewable
     *
     * @return bool
     */
    public function isViewable()
    {
        foreach ($this->steps as $step) {
            if (!$step->isFinished() && $step->isRequiredForView()) {
                return false;
            }
        }

        return true;
    }

    /**
     * getStepAfter
     *
     * @param string $stepKey
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function getStepAfter($stepKey)
    {
        if (!$this->isValidStepKey($stepKey)) {
            throw new \InvalidArgumentException('Invalid step');
        }

        reset($this->steps);

        while ($step = current($this->steps)) {
            if ($step->getKey() !== $stepKey) {
                next($this->steps);
                continue;
            }

            $next = next($this->steps);
            if (!$next) {
                return null;
            }

            return $next;
        }
    }

    /**
     * isStepAccessible
     *
     * @param string $stepKey
     * @return bool
     */
    public function isStepAccessible($stepKey)
    {
        $unfinished = $this->getFirstUnfinishedStep();
        if (!$unfinished) {
            return true;
        }

        $lastKey = $unfinished->getKey();

        foreach ($this->steps as $step) {
            $thisKey = $step->getKey();

            if ($thisKey === $stepKey) {
                return true;
            }

            if ($thisKey === $lastKey) {
                return false;
            }
        }
    }

    /**
     * isValidStepKey
     *
     * @param string $stepKey
     * @return bool
     */
    public function isValidStepKey($stepKey)
    {
        foreach ($this->steps as $step) {
            if ($step->getKey() === $stepKey) {
                return true;
            }
        }

        return false;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'brown298.report_builder.build.step';
    }

    /**
     * createForm
     *
     * @param  TreeMetadata $treeMeta
     * @param  SecurityInterface $security
     * @param  array $classes
     * @throws Exception
     * @return object
     */
    public function createForm(
        TreeMetadata      $treeMeta,
        SecurityInterface $security,
                          $classes
    ) {
        $step    = $this->getCurrentStep();
        $stepKey = $step->getKey();
        $report  = $this->reportService->getReport();

        // Form
        $class = DoctrineInflector::classify($stepKey);
        $class = sprintf(self::STEP_FORM_CLASS, $class);

        if (!class_exists($class)) {
            $message = sprintf('Class %s does not exist', $class);
            throw new Exception($message);
        }

        $form = new $class($report, $treeMeta, $security);

        if ($stepKey === 'entity') {
            $form->setClasses($classes);
        }

        return $form;
    }
}