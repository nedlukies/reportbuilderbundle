<?php
namespace Brown298\ReportBuilderBundle\Service;

/**
 * Class TemplateService
 * @package Brown298\ReportBuilderBundle\Service
 */
class TemplateService
{
    /**
     * @var string
     */
    protected $buildTemplate;

    /**
     * @var string
     */
    protected $indexTemplate;

    /**
     * @var string
     */
    protected $filterFormTemplate;

    /**
     * @var string
     */
    protected $errorSpanTemplate;

    /**
     * @var string
     */
    protected $viewTemplate;

    public function __construct($buildTemplate, $indexTemplate, $filterFormTemplate, $errorSpanTemplate, $viewTemplate)
    {
        $this->buildTemplate      = $buildTemplate;
        $this->indexTemplate      = $indexTemplate;
        $this->filterFormTemplate = $filterFormTemplate;
        $this->errorSpanTemplate  = $errorSpanTemplate;
        $this->viewTemplate       = $viewTemplate;
    }

    /**
     * @param $stepKey
     * @return string
     */
    public function getTemplateByStep($stepKey)
    {
        return sprintf($this->buildTemplate, $stepKey);
    }

    /**
     * @return string
     */
    public function getIndexTemplate()
    {
        return $this->indexTemplate;
    }

    /**
     * @return string
     */
    public function getFilterFormTemplate()
    {
        return $this->filterFormTemplate;
    }

    /**
     * @return string
     */
    public function getErrorSpanTemplate()
    {
        return $this->errorSpanTemplate;
    }

    /**
     * @return string
     */
    public function getViewTemplate()
    {
        return $this->viewTemplate;
    }


}